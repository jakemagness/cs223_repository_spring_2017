/** PEX1.c
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: PEX1.
 * Purpose: Perform timing of certain operations.
 * Doc: Basis of code was provided by Dr. Brown.  Dr. Brown
 * also assisted with fixing my selection sort algorithm when
 * viewing this code.
 * ===========================================================
 */


#include "ArrayList.h"
#include "LinkedList.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main() {
    // Declare the functions that are called.
    void timeArrayListCreation();
    void timeArrayListAppend();
    void timeArrayListAppend10();
    void timeLinkedListAppend();
    void timeArrayListInsertionSort();
    void timeLinkedListSort();
    void linkedListMiddle();

    // Perform the timing code
    timeArrayListCreation();
    timeArrayListAppend();
    timeArrayListAppend10();
    timeLinkedListAppend();
    timeArrayListInsertionSort();
    timeLinkedListSort();
    linkedListMiddle();
}

/** ----------------------------------------------------------
* Find the time to create an array list.
* @return None.
*/
void timeArrayListCreation() {
    ArrayList *my_list;
    clock_t    startTime;
    clock_t    elapsedTime;
    FILE      *fp;

    fp = fopen("PEXs/PEX1/timesForArrayListCreate.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {
        startTime = clock();

        my_list = arrayListCreate(size);

        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        arrayListDelete(my_list);
    }
    fclose(fp);
}

/** ----------------------------------------------------------
* Find the time to append elements to an array list struct.
* @return None.
*/
void timeArrayListAppend() {

    ArrayList *my_list;
    clock_t startTime;
    clock_t elapsedTime;
    FILE *fp;
    ElementType randomValue = 0;

    fp = fopen("PEXs/PEX1/timesForArrayListAppend.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {
        startTime = clock();

        my_list = arrayListCreate(size);

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            arrayListAppend(my_list, &randomValue);
        }


        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        arrayListDelete(my_list);
    }
    fclose(fp);
}

/** ----------------------------------------------------------
* Find the time to append elements to an array list struct of size 10.
* @return None.
*/
void timeArrayListAppend10() {
    ArrayList *my_list;
    clock_t startTime;
    clock_t elapsedTime;
    FILE *fp;
    ElementType randomValue = 0;

    fp = fopen("PEXs/PEX1/timesForArrayListAppend10.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {
        startTime = clock();

        my_list = arrayListCreate(10);

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            arrayListAppend(my_list, &randomValue);
        }

        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        arrayListDelete(my_list);
    }
    fclose(fp);
}

/** ----------------------------------------------------------
* Find the time to append elements to a linked list struct.
* @return None.
*/
void timeLinkedListAppend() {
    LinkedList *my_list;
    clock_t startTime;
    clock_t elapsedTime;
    FILE *fp;
    ElementType randomValue = 0;

    fp = fopen("PEXs/PEX1/timesForLinkedListAppend.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {
        startTime = clock();

        my_list = linkedListCreate();

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            linkedListAppend(my_list, &randomValue);
        }

        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        linkedListDelete(my_list);
    }
    fclose(fp);
}

/** ----------------------------------------------------------
* Find the time to perform an arraylist insertion sort.
* @return None.
*/
void timeArrayListInsertionSort() {
    ArrayList *my_list;
    clock_t startTime;
    clock_t elapsedTime;
    FILE *fp;
    ElementType randomValue = 0;

    fp = fopen("PEXs/PEX1/timesForArrayListSort.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {

        my_list = arrayListCreate(size);

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            arrayListAppend(my_list, &randomValue);
        }

        startTime = clock();
        arrayListInsertionSort(my_list);

        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        arrayListDelete(my_list);
    }
    fclose(fp);
}

/** ----------------------------------------------------------
* Find the time to perform a linked list selection sort.
* @return None.
*/
void timeLinkedListSort() {
    LinkedList *my_list;
    clock_t startTime;
    clock_t elapsedTime;
    FILE *fp;
    ElementType randomValue = 0;

    fp = fopen("PEXs/PEX1/timesForLinkedListSort.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {

        my_list = linkedListCreate();

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            linkedListAppend(my_list, &randomValue);
        }

        startTime = clock();

        linkedListSelectionSort(my_list);

        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        linkedListDelete(my_list);
    }
    fclose(fp);
}

/** ----------------------------------------------------------
* Find the time to insert elements into the middle of a linked list.
* @return None.
*/
void linkedListMiddle(){
    LinkedList *my_list;
    clock_t startTime;
    clock_t elapsedTime;
    FILE *fp;
    ElementType randomValue = 0;

    fp = fopen("PEXs/PEX1/timesForLinkedListMiddle.csv", "w");

    for (int size = 0; size < 100000; size += 10000) {


        my_list = linkedListCreate();

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            linkedListAppend(my_list, &randomValue);
        }

        startTime = clock();

        for(int i = 0; i < size; i++){
            randomValue = (int) ((float) rand() / (float) RAND_MAX * size);
            linkedListInsertElement(my_list, size/2, randomValue);
        }

        elapsedTime = clock() - startTime;

        printf("%d, %lu\n", size, elapsedTime);
        fprintf(fp, "%d, %lu\n", size, elapsedTime);

        // Delete the array list so that we will not run out of memory
        linkedListDelete(my_list);
    }
    fclose(fp);
}
