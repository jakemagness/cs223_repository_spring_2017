/** verifyArrayList.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section: M3A
 * Project: Unit tests for an ArrayList
 * Purpose: Software verification. (Does the software work correctly?)
 * ===========================================================
 */

#include "ArrayList.h"
#include "LinkedList.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define FALSE  0
#define TRUE   1

// Function declarations
int compareLists(ArrayList *list, ElementType *array, int n);
void verifyAppend();
void verifyGetElement();
void verifyDeleteElement();
void verifyInsertElement();
void verifyChangeElement();
void verifyFindElement();

// The default test list
int testArray[] = {3,6,4,1,8};

/** -------------------------------------------------------------------
 * Run a series of tests on an ArrayList. If no error messages are
 * printed, the ArrayList has minimal errors. (This set of
 * tests is not guaranteed to find all possible errors.)
 * @return
 */
int main() {
    verifyAppend();
    verifyGetElement();
    verifyDeleteElement();
    verifyInsertElement();
    verifyChangeElement();
    verifyFindElement();

    ArrayList *myList;
    myList = arrayListCreate(10);
    ElementType number = 0;

    //Code Below used to test the Insertion Sort
//    for(int i = 0; i < 10; i++){
//        number = (ElementType) ((float) rand() / (float) RAND_MAX * 10);
//        arrayListAppend(myList, &number);
//    }
//    for(int j = 0; j < 10; j++){
//        printf("%d ", myList->array[j]);
//    }
//    printf("\n");
//    arrayListInsertionSort(myList);
//
//    for(int j = 0; j < 10; j++){
//        printf("%d ", myList->array[j]);
//    }

    exit(0);
}

/** -------------------------------------------------------------------
 * Compare an ArrayList to a simple array and return if they have equivalent contents
 * @param list - an ArrayList
 * @param array - a simple array
 * @param n - the number of elements in the simple array
 * @return - TRUE if the ArrayList and the array contain the same values in the same order.
 */
int compareLists(ArrayList *list, ElementType *array, int n) {
    for (int j = 0; j < n; j++) {
        if (arrayListGetElement(list, j) != array[j]) {
            return FALSE;
        }
    }
    return TRUE;
}

/** -------------------------------------------------------------------
 * Create a test array with a known contents.
 * @return a pointer to an ArrayList
 */
ArrayList * createTestArray() {
    ArrayList *myList;

    myList = arrayListCreate(10);
    for (int j = 0; j < 5; j++) {
        arrayListAppend(myList, &testArray[j]);
    }
    return myList;
}

/** -------------------------------------------------------------------
 * Verify that the arrayListAppend() function works.
 */
void verifyAppend() {
    #define NUM_APPEND_TESTS  3
    typedef struct testCase {
        int    value;
        int    expectedResult[10];
    } TestCase;

    TestCase  tests[] = { {3, {3} },
                          {6, {3,6} },
                          {2, {3,6,2} }
                        };
    ArrayList * myList;

    printf("verifying arrayListAppend() - only prints errors\n");

    myList = arrayListCreate(10);
    for (int j=0; j < NUM_APPEND_TESTS; j++) {
        arrayListAppend(myList, &(tests[j].value));
        if (! compareLists(myList, tests[j].expectedResult, j+1)) {
            printf("Test %d failed for arrayListAppend\n", j);
        }
    }
    arrayListDelete(myList);
}

/** -------------------------------------------------------------------
 * Verify that the arrayListGetElement() function works
 */
void verifyGetElement() {
    #define NUM_GET_TESTS  3
    ArrayList * myList;
    int testPositions[] = { 0, 2, 4};
    int expectedValue[] = { 3, 4, 8};
    int returnedValue;

    printf("verifying arrayListGetElement() - only prints errors\n");

    // Build a test ArrayList
    myList = createTestArray();

    // Test the ArrayList at the first, middle, and last positions
    for (int j=0; j < NUM_GET_TESTS; j++) {
        returnedValue = arrayListGetElement(myList, testPositions[j]);
        if (returnedValue != expectedValue[j]) {
            printf("Test %d failed for arrayListGetElement, expected %d but got %d\n",
                   j, expectedValue[j], returnedValue);
        }
    }
    arrayListDelete(myList);
}

/** -------------------------------------------------------------------
 * Verify that the arrayListDeleteElement() function works
 */
void verifyDeleteElement() {
    #define NUM_DELETE_TESTS  3
    typedef struct testCase {
        int    position;
        int    expectedResult[10];
    } TestCase;

    //int testArray[] = {3,6,4,1,8};
    TestCase  tests[] = { {0, {6,4,1,8} },
                          {2, {3,6,1,8} },
                          {4, {3,6,4,1} }
    };
    ArrayList * myList;

    printf("verifying arrayListDeleteElement() - only prints errors\n");

    for (int j=0; j < NUM_DELETE_TESTS; j++) {
        myList = createTestArray();
        arrayListDeleteElement(myList, tests[j].position);
        if (! compareLists(myList, tests[j].expectedResult, 4)) {
            printf("Test %d failed for arrayListDeleteElement\n", j);
        }
        arrayListDelete(myList);
    }
}

/** -------------------------------------------------------------------
 * Verify that the arrayListInsertElement() function works
 */
void verifyInsertElement() {
    #define NUM_INSERT_TESTS  4
    typedef struct testCase {
        int    position;
        int    value;
        int    expectedResult[10];
    } TestCase;

    //int testArray[] = {3,6,4,1,8};
    TestCase  tests[] = { {0, 37, {37,3,6,4,1,8} },
                          {2, 37, {3,6,37,4,1,8} },
                          {4, 37, {3,6,4,1,37,8} },
                          {5, 38, {3,6,4,1,8,38} }
    };
    ArrayList * myList;

    printf("verifying arrayListInsertElement() - only prints errors\n");

    for (int j=0; j < NUM_INSERT_TESTS; j++) {
        myList = createTestArray();
        arrayListInsertElement(myList, tests[j].position, tests[j].value);
        if (! compareLists(myList, tests[j].expectedResult, 6)) {
            printf("Test %d failed for arrayListInsertElement\n", j);
        }
        arrayListDelete(myList);
    }
}

/** -------------------------------------------------------------------
 * Verify that the arrayListChangeElement() function works
 */
void verifyChangeElement() {
    #define NUM_CHANGE_TESTS  3
    typedef struct testCase {
        int    position;
        int    newValue;
        int    expectedResult[10];
    } TestCase;

    //int testArray[] = {3,6,4,1,8};
    TestCase  tests[] = { {0, 37, {37,6,4,1,8} },
                          {2, 38, {3,6,38,1,8} },
                          {4, 39, {3,6,4,1,39} }
    };
    ArrayList * myList;

    printf("verifying arrayListChangeElement() - only prints errors\n");

    for (int j=0; j < NUM_CHANGE_TESTS; j++) {
        myList = createTestArray();
        arrayListChangeElement(myList, tests[j].position, tests[j].newValue);
        if (! compareLists(myList, tests[j].expectedResult, 5)) {
            printf("Test %d failed for arrayListChangeElement\n", j);
        }
        arrayListDelete(myList);
    }
}

/** -------------------------------------------------------------------
 * Verify that the arrayListFindElement() function works.
 */
void verifyFindElement() {
    #define NUM_FIND_TESTS  4
    //int testArray[] = {3,6,4,1,8};
    int testValues[]    = { 3, 4, 8, 37};
    int expectedValue[] = { 0, 2, 4, -1};
    int returnedValue;
    ArrayList * myList;

    printf("verifying arrayListFindElement() - only prints errors\n");

    for (int j=0; j < NUM_FIND_TESTS; j++) {
        myList = createTestArray();
        returnedValue = arrayListFindElement(myList, testValues[j]);
        if (returnedValue != expectedValue[j]) {
            printf("Test %d failed for arrayListFindElement\n", j);
        }
        arrayListDelete(myList);
    }
}



