/** ArrayList.h
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a list using an array.
 * Purpose: General purpose list implementation.
 * Doc: Basis of code was provided by Dr. Brown.  Used online
 * resources to help me build the Insertion Sort algorithim.
 * ===========================================================
 */

#ifndef ArrayList_H
#define ArrayList_H

// Define the datatype for each element of the list.
typedef int ElementType;

// Define the structure that holds the meta-data for one list.
typedef struct ArrayList {
    ElementType   *array;
    int             array_size;
    int             number_elements;
} ArrayList;

// Define the functions that manipulate an ArrayList
ArrayList *arrayListCreate(int initial_size);
void arrayListDelete(ArrayList *list);
void arrayListAppend(ArrayList *list, ElementType *element);
void arrayListPrint(ArrayList *list);
ElementType arrayListGetElement(ArrayList *list, int index);
void arrayListDeleteElement(ArrayList *list, int index);
void arrayListInsertElement(ArrayList *list, int position, ElementType value);
void arrayListChangeElement(ArrayList *list, int position, ElementType newValue);
int arrayListFindElement(ArrayList *list, ElementType value);
int arrayListSize(ArrayList *list);

void arrayListSelectionSort(ArrayList *list);
void arrayListInsertionSort(ArrayList *list);

#endif // ArrayList_H
