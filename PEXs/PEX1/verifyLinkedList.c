/** verifyLinkedList.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section: M3A
 * Project: Unit tests for a LinkedList
 * Purpose: Software verification. (Does the software work correctly?)
 * ===========================================================
 */

#include "LinkedList.h"
#include "LinkedList.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define FALSE  0
#define TRUE   1

// Function declarations
int compareLists(LinkedList *list, ElementType *array, int n);
LinkedList * createTestArray();
void verifyAppend();
void verifyGetElement();
void verifyDeleteElement();
void verifyInsertElement();
void verifyFindElement();
void verifyChangeElement();


// The default test list
int testArray[] = {3,6,4,1,8};

/** -------------------------------------------------------------------
 * Run a series of tests on a LinkedList. If no error messages are
 * printed, the LinkedList has minimal errors. (This set of
 * tests is not guaranteed to find all possible errors.)
 * @return program error code
 */
int main() {
    verifyAppend();
    verifyGetElement();
    verifyDeleteElement();
    verifyInsertElement();
    verifyChangeElement();
    verifyFindElement();


    //Code used to test selection sort function.
//    ElementType randomValue = 0;
//    LinkedList *a = linkedListCreate();
//
//    for(int i = 0; i < 10000; i++){
//        randomValue = (int) ((float) rand() / (float) RAND_MAX * 10);
//        linkedListAppend(a, &randomValue);
//    }
//
//    linkedListSelectionSort(a);
//    linkedListPrint(a);

    exit(0);
}

/** -------------------------------------------------------------------
 * Compare a LinkedList to a simple array and return if they have equivalent contents
 * @param list - a LinkedList
 * @param array - a simple array
 * @param n - the number of elements in the simple array
 * @return - TRUE if the LinkedList and the array contain the same values in the same order.
 */
int compareLists(LinkedList *list, ElementType *array, int n) {
    for (int j = 0; j < n; j++) {
        if (linkedListGetElement(list, j) != array[j]) {
            return FALSE;
        }
    }
    return TRUE;
}

/** -------------------------------------------------------------------
 * Create a test array with a known contents.
 * @return a pointer to a LinkedList
 */
LinkedList * createTestArray() {
    LinkedList *myList;

    myList = linkedListCreate();
    for (int j = 0; j < 5; j++) {
        linkedListAppend(myList, &testArray[j]);
    }
    return myList;
}

/** -------------------------------------------------------------------
 * Verify that the linkedListAppend() function works.
 */
void verifyAppend() {
    #define NUM_APPEND_TESTS  3
    typedef struct testCase {
        int    value;
        int    expectedResult[10];
    } TestCase;

    TestCase  tests[] = { {3, {3} },
                          {6, {3,6} },
                          {2, {3,6,2} }
                        };
    LinkedList * myList;

    printf("verifying linkedListAppend() - only prints errors\n");

    myList = linkedListCreate();
    for (int j=0; j < NUM_APPEND_TESTS; j++) {
        linkedListAppend(myList, &(tests[j].value));
        if (! compareLists(myList, tests[j].expectedResult, j+1)) {
            printf("Test %d failed for linkedListAppend\n", j);
        }
    }
    linkedListDelete(myList);
}

/** -------------------------------------------------------------------
 * Verify that the linkedListGetElement() function works
 */
void verifyGetElement() {
    #define NUM_GET_TESTS  3
    LinkedList * myList;
    int testPositions[] = { 0, 2, 4};
    int expectedValue[] = { 3, 4, 8};
    int returnedValue;

    printf("verifying linkedListGetElement() - only prints errors\n");

    // Build a test LinkedList
    myList = createTestArray();

    // Test the LinkedList at the first, middle, and last positions
    for (int j=0; j < NUM_GET_TESTS; j++) {
        returnedValue = linkedListGetElement(myList, testPositions[j]);
        if (returnedValue != expectedValue[j]) {
            printf("Test %d failed for LinkedListGetElement, expected %d but got %d\n",
                   j, expectedValue[j], returnedValue);
        }
    }
    linkedListDelete(myList);
}

/** -------------------------------------------------------------------
 * Verify that the linkedListDeleteElement() function works
 */
void verifyDeleteElement() {
    #define NUM_DELETE_TESTS  3
    typedef struct testCase {
        int    position;
        int    expectedResult[10];
    } TestCase;

    //int testArray[] = {3,6,4,1,8};
    TestCase  tests[] = { {0, {6,4,1,8} },
                          {2, {3,6,1,8} },
                          {4, {3,6,4,1} }
    };
    LinkedList * myList;

    printf("verifying linkedListDeleteElement() - only prints errors\n");

    for (int j=0; j < NUM_DELETE_TESTS; j++) {
        myList = createTestArray();
        linkedListDeleteElement(myList, tests[j].position);
        if (! compareLists(myList, tests[j].expectedResult, 4)) {
            printf("Test %d failed for linkedListDeleteElement\n", j);
        }
        linkedListDelete(myList);
    }
}

/** -------------------------------------------------------------------
 * Verify that the linkedListInsertElement() function works
 */
void verifyInsertElement() {
    #define NUM_INSERT_TESTS  4
    typedef struct testCase {
        int    position;
        int    value;
        int    expectedResult[10];
    } TestCase;

    //int testArray[] = {3,6,4,1,8};
    TestCase  tests[] = { {0, 37, {37,3,6,4,1,8} },
                          {2, 37, {3,6,37,4,1,8} },
                          {4, 37, {3,6,4,1,37,8} },
                          {5, 38, {3,6,4,1,8,38} }
    };
    LinkedList * myList;

    printf("verifying linkedListInsertElement() - only prints errors\n");

    for (int j=0; j < NUM_INSERT_TESTS; j++) {
        myList = createTestArray();
        linkedListInsertElement(myList, tests[j].position, tests[j].value);
        if (! compareLists(myList, tests[j].expectedResult, 6)) {
            printf("Test %d failed for linkedListInsertElement\n", j);
        }
        linkedListDelete(myList);
    }
}

/** -------------------------------------------------------------------
 * Verify that the linkedListChangeElement() function works
 */
void verifyChangeElement() {
    #define NUM_CHANGE_TESTS  3
    typedef struct testCase {
        int    position;
        int    newValue;
        int    expectedResult[10];
    } TestCase;

    //int testArray[] = {3,6,4,1,8};
    TestCase  tests[] = { {0, 37, {37,6,4,1,8} },
                          {2, 38, {3,6,38,1,8} },
                          {4, 39, {3,6,4,1,39} }
    };
    LinkedList * myList;

    printf("verifying linkedListChangeElement() - only prints errors\n");

    for (int j=0; j < NUM_CHANGE_TESTS; j++) {
        myList = createTestArray();
        linkedListChangeElement(myList, tests[j].position, tests[j].newValue);
        if (! compareLists(myList, tests[j].expectedResult, 5)) {
            printf("Test %d failed for LinkedListChangeElement\n", j);
        }
        linkedListDelete(myList);
    }
}

/** -------------------------------------------------------------------
 * Verify that the linkedListFindElement() function works.
 */
void verifyFindElement() {
    #define NUM_FIND_TESTS  4
    //int testArray[] = {3,6,4,1,8};
    int testValues[]    = { 3, 4, 8, 37};
    int expectedValue[] = { 0, 2, 4, -1};
    int returnedValue;
    LinkedList * myList;

    printf("verifying linkedListFindElement() - only prints errors\n");

    for (int j=0; j < NUM_FIND_TESTS; j++) {
        myList = createTestArray();
        returnedValue = linkedListFindElement(myList, testValues[j]);
        if (returnedValue != expectedValue[j]) {
            printf("Test %d failed for linkedListFindElement\n", j);
        }
        linkedListDelete(myList);
    }
}



