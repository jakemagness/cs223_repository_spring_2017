/** PEX2.c
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a Ternary Tree data type.
 * Purpose: General purpose Ternary Tree implementation.
 * Doc: None.
 * ===========================================================
 */

#include "Wordlist.h"
#include "TernaryTree.h"
#include <stdio.h>
#include <stdlib.h>  // Defines malloc and free
#include <string.h>

// Function Definitions.
TernaryTree * ternaryTreeCreate();
void ternaryTreeDelete(TernaryTree * tree);
void ternaryTreeInsert(TernaryTree * tree, char * word);
WordList * ternaryTreeFindPattern(TernaryTree * tree, char * pattern);
void stripNewline(char *word);
WordList * loadDictionary();
void stripNewLine(char * word);
int ternaryTreeBalanced(TernaryTree * tree, WordList * word, int first, int last);
int ternaryTreeBalancedTest(TernaryTree * tree, WordList * word, int first, int last);

int main(){
    TernaryTree * r;

    r = (TernaryTree *) malloc(sizeof(TernaryTree));

    WordList * words;
    words = wordListCreate();
    words = loadDictionary();

    ternaryTreeBalanced(r, words, 0, wordListSize(words));

    ternaryTreeBalancedTest(r, words, 0, wordListSize(words));
    ternaryTreeInsertTest(r, wordListGet(words, 0));
    ternaryTreePatternTest(r, "CAR");


    return 0;
}