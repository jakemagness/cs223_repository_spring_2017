/** ternaryTreeUnitTest.c
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a Ternary Tree test functions.
 * Purpose: Implements basic Ternary Tree test functions. .
 * Doc: None.
 * ===========================================================
 */

#include <stdio.h>
#include <stdlib.h>  // Defines malloc and free
#include <string.h>
#include <ctype.h>

#include "TernaryTree.h"


int ternaryTreeBalancedTest(TernaryTree * tree, WordList * word, int first, int last){
    ternaryTreeBalanced(tree, word, first, last);

    //Add code here to gather what is actually being put in. Then create table below to display outputs.
    //Change dictionaries in the load dictionary function to change the lists to be tested.

    printf("\n\n%s", "Balanced Test \n");
    char * printWords[10];
    printWords[0] = "Inserting 'COP'";
    printWords[1] = "Inserting 'CAR'";
    printWords[2] = "Inserting 'CAD'";
    printWords[3] = "Inserting 'CAB'";
    printWords[4] = "Inserting 'CAT'";
    printWords[5] = "Inserting 'COW'";
    printWords[6] = "Inserting 'COT'";
    printWords[7] = "Inserting 'CUT'";
    printf("\n");
    printf("%s\t\t\t%s\n","INPUTS:", "Expected Outputs");
    for(int i = 0; i < 8; i++){
        printf("\t\t\t%s\n", printWords[i]);
    }
    printf("%s\n", "");
    return 0;
}

int ternaryTreeInsertTest(TernaryTree * tree, char * word){
    ternaryTreeInsert(tree, word);
    //Add code here to gather what is actually being put in. Then change/create table below to display outputs.
    //Change dictionaries in the load dictionary function to change the lists to be tested.

    printf("\n\n%s", "Insertion Test \n");
    char * printWords[10];
    printWords[0] = "Inserting 'COP'";
    printWords[1] = "Inserting 'CAR'";
    printWords[2] = "Inserting 'CAD'";
    printWords[3] = "Inserting 'CAB'";
    printWords[4] = "Inserting 'CAT'";
    printWords[5] = "Inserting 'COW'";
    printWords[6] = "Inserting 'COT'";
    printWords[7] = "Inserting 'CUT'";
    printf("\n");
    printf("%s\t\t\t%s\n","INPUTS:", "Expected Outputs");
    for(int i = 0; i < 8; i++){
        printf("\t\t\t%s\n", printWords[i]);
    }
    printf("%s\n", "");
    return 0;
}

int ternaryTreePatternTest(TernaryTree * tree, char * pattern){
//    ternaryTreeFindPattern(tree, pattern);

    //Add code here to gather what is actually being put in. Then change/create table below to display outputs.
    //Change dictionaries in the load dictionary function to change the lists to be tested.

    printf("\n\n%s%s", "Pattern Search \n", pattern);
    printf("%s", "Pattern Found: PATTERN");
    printf("%s", "");
    return 0;
}