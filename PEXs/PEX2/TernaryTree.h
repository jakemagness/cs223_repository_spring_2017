/** TernaryTree.h
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a Ternary Tree data type.
 * Purpose: General purpose Ternary Tree implementation.
 * Doc: None.
 * ===========================================================
 */

#ifndef CS223_REPOSITORY_SPRING_2017_TERNARYTREE_H
#define CS223_REPOSITORY_SPRING_2017_TERNARYTREE_H

#include "Wordlist.h"


// Structure Definition
typedef struct TernaryTree{
    char  data;
    struct TernaryTree * firstChild;
    struct TernaryTree * secondChild;
    struct TernaryTree * thirdChild;
}TernaryTree;

// Function Definitions.
TernaryTree * ternaryTreeCreate();
void ternaryTreeDelete(TernaryTree * tree);
void ternaryTreeInsert(TernaryTree * tree, char * word);
WordList * ternaryTreeFindPattern(TernaryTree * tree, char * pattern);
void stripNewline(char *word);
WordList * loadDictionary();
void stripNewLine(char * word);
int ternaryTreeBalanced(TernaryTree * tree, WordList * word, int first, int last);
int ternaryTreeBalancedTest(TernaryTree * tree, WordList * word, int first, int last);
int ternaryTreeInsertTest(TernaryTree * tree, char * word);
int ternaryTreePatternTest(TernaryTree * tree, char * pattern);

#endif //CS223_REPOSITORY_SPRING_2017_TERNARYTREE_H

