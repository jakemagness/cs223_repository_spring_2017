/** TernaryTree.c
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a Ternary Tree data type and functions.
 * Purpose: General purpose Ternary Tree implementation.
 * Doc: Saw Dr. Brown for EI and got help for inserting balanced values.
 * ===========================================================
 */
#include <stdio.h>
#include <stdlib.h>  // Defines malloc and free
#include <string.h>
#include <ctype.h>

#include "TernaryTree.h"
static int j = 0;
WordList * sortedList;

TernaryTree * ternaryTreeCreate(){
    TernaryTree * newTree;
    newTree = (TernaryTree *) malloc(sizeof(TernaryTree));
    newTree->data = '0';
    newTree->firstChild = NULL;
    newTree->secondChild = NULL;
    newTree->thirdChild = NULL;
    return newTree;
}

void stripNewline(char *word){
    char *newLine;
    if ((newLine = strchr(word, '\n')) != NULL) {
        *newLine = '\0';
    }
}

void ternaryTreeDelete(TernaryTree * tree){
    //Can't find appropriate function to properly gather the size of a struct.  This function is what worked earlier.
    memset(tree, 0, 100);
}

WordList * loadDictionary(){
    FILE * fp;
    char * buffer;

    WordList * words;
    words = wordListCreate();

    buffer = (char *) malloc(sizeof(char) * (20));

    fp = fopen("PEXs/PEX2/test_dict1.txt", "r");

    while (fgets(buffer,100, fp)!=NULL){
        wordListAdd(words, buffer);
    }

    fclose(fp);

    return words;
}

int ternaryTreeBalanced(TernaryTree * tree, WordList * word, int first, int last){

    if (word == NULL || first > last) {
        return -1;
    } else {
        int middle = (first + 1 + last) / 2;
        j = 0;
        ternaryTreeInsert(tree, wordListGet(word, middle));
        ternaryTreeBalanced(tree, word, first, middle - 1);
        ternaryTreeBalanced(tree, word, middle + 1, last);
    }
    return 0;
}

void ternaryTreeInsert(TernaryTree * tree, char * word){

    TernaryTree *newTree = (TernaryTree *) malloc(sizeof(TernaryTree));

    memset(newTree, 0 , 32); //For some reason getting error where my newTree has values in it.
    if(word != NULL) {
        size_t f = strlen(word);
        stripNewline(word);
        while (j < f) {
            if (word[j] == '\r') {
                if (tree->secondChild == NULL) {
                    tree->secondChild = newTree;
                    tree->secondChild->data = '\0';
                } else if (tree->secondChild->data != '0') {
                    if (tree->thirdChild == NULL) {
                        tree->thirdChild = newTree;
                        tree->thirdChild->data = '\0';
                        if (tree->thirdChild->data == '0') {
                            tree->thirdChild->data = '\0';
                        }
                    } else if (tree->firstChild == NULL) {
                        tree->firstChild = newTree;
                        tree->firstChild->data = '\0';
                        if (tree->firstChild->data == '0') {
                            tree->firstChild->data = '\0';
                        }
                    }
                } else {
                    tree->secondChild->data = '\0';
                }
            } else if (tree->data == 0) {
                tree->data = word[j];
                tree->secondChild = newTree;
                j++;
                ternaryTreeInsert(tree->secondChild, word);
            } else if (tree->data == word[j]) {
                j++;
                if (tree->secondChild != NULL) {
                    ternaryTreeInsert(tree->secondChild, word);
                } else {
                    tree->secondChild = newTree;
                    ternaryTreeInsert(tree->secondChild, word);
                }
            } else if (tree->secondChild == NULL) {
                tree->secondChild = newTree;
                ternaryTreeInsert(tree->secondChild, word);
            } else {
                if (tree->data > word[j]) {
                    if (tree->firstChild == NULL) {
                        newTree->data = word[j];
                        tree->firstChild = newTree;
                        j++;
                        ternaryTreeInsert(tree->firstChild, word);
                    } else {
                        ternaryTreeInsert(tree->firstChild, word);
                    }
                } else if (tree->data < word[j]) {
                    if (tree->thirdChild == NULL) {
                        tree->thirdChild = newTree;
                        newTree->data = word[j];
                        j++;
                        ternaryTreeInsert(tree->thirdChild, word);
                    } else {
                        ternaryTreeInsert(tree->thirdChild, word);
                    }
                }
            }
            j++;
        }
    }
}
// Have had very little time to test this function.
WordList * ternaryTreeFindPattern(TernaryTree * tree, char * pattern){
    WordList * list = wordListCreate();
    static int i = 0;
    char word[20];

    while(i<sizeof(pattern+1)){
        if(tree == NULL){
            return NULL;
        }
        if(tree->data == '\0'){
            wordListAdd(list, word);
        }
        else{
            if(tree->data == pattern[i]){
                word[i] = pattern[i];
                if(tree->firstChild->data == pattern[i+1]){
                    i++;
                    list = ternaryTreeFindPattern(tree->firstChild, pattern);
                }
                else if(tree->secondChild->data == pattern[i+1]){
                    i++;
                    list = ternaryTreeFindPattern(tree->secondChild, pattern);
                }
                else{
                    i++;
                    list = ternaryTreeFindPattern(tree->thirdChild, pattern);
                }
            }
            else{
                if(tree->data < pattern[i]){
                    ternaryTreeFindPattern(tree->firstChild, pattern);
                }
                else{
                    ternaryTreeFindPattern(tree->thirdChild, pattern);
                }
            }

        }
    }
}

