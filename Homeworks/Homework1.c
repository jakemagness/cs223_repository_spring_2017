//
// Created by C18Jake.Magness on 1/23/2017.
// Jake Magness
// 31 Jan 2017
// CS223
// Doc: Used online resources for question 2.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    int first = 0;
    unsigned int second = 0;
    float third = 0;
    double fourth = 0;
    short fifth = 0;
    char sixth = 0;
    unsigned char seventh = 0;
    long eight = 0;
    long long ninth = 0;
    char alpha1[20];
    int beta[30];
    typedef struct gamma{
        int field1;
        float field2;
        char field3[10];
    }gamma1;
    int * int_pointer = 0;
    float * float_pointer = 0;
    double * double_pointer = 0;
    char * char_pointer = 0;
    long * long_pointer = 0;


    int n = sizeof(first);
    printf("One:%d\n", n);

    n = sizeof(second);
    printf("Two:%d\n", n);

    n = sizeof(third);
    printf("Three:%d\n", n);

    n = sizeof(fourth);
    printf("Fourth:%d\n", n);

    n = sizeof(fifth);
    printf("Fifth:%d\n", n);

    n = sizeof(sixth);
    printf("Sixth:%d\n", n);

    n = sizeof(seventh);
    printf("seventh:%d\n", n);

    n = sizeof(eight);
    printf("Eighth:%d\n", n);

    n = sizeof(ninth);
    printf("Ninth:%d\n", n);

    n = sizeof(alpha1);
    printf("Alpha[20]:%d\n", n);

    n = sizeof(beta);
    printf("Beta[30]:%d\n", n);

    n = sizeof(gamma1);
    printf("Gamma:%d\n", n);

    n = sizeof(int_pointer);
    printf("Int pointer:%d\n", n);

    n = sizeof(float_pointer);
    printf("Float Pointer:%d\n", n);

    n = sizeof(double_pointer);
    printf("Double Pointer:%d\n", n);

    n = sizeof(char_pointer);
    printf("Char Pointer:%d\n", n);

    n = sizeof(long_pointer);
    printf("Long Pointer:%d\n", n);

    gamma1 * example;
    n = sizeof(example);
    printf("Gamma Pointer:%d\n", n);



    int alpha[6] = {10, 20, 30, 40};
    char courseName[] = "CS223 Data Structures and System Programming";
    char aCharacter;

    alpha[0] = alpha[1] + 2;    // alpha = 22
    printf("alpha[0]:%i\n", alpha[0]);

    aCharacter = courseName[3];  // aCharacter = 50
    printf("aCharacter:%d\n", aCharacter);

    courseName[5] = '_';        // courseName = 95
    printf("courseName[5]:%d\n", courseName[5]);

    strcpy(courseName + 6, "DataStructures"); // courseName = -13632 when printing d. Printing string prints
    printf("String:%s\n", courseName); // CS223_DataStructures

    alpha[10] = 2;               // alpha = 2
    printf("Alpha[10]:%d\n", alpha[10]);

    *(alpha + 3) = -3;           // alpha = -13584 with decimal, 0xffffcaf0 with pointer.
    printf("Alpha:%p\n", alpha);

    return 0;
}

//5
//a) O(log(2) n), binary search
//b) O(n), linear search
//c) O(n), linear search
//d) O(1)