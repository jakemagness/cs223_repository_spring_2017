cmake_minimum_required(VERSION 3.6)
project(CS223_repository_spring_2017)

set(CMAKE_C_STANDARD 11)

set(SOURCE_FILES Labs/Experiments/print_data_types.c)
# set(SOURCE_FILES Labs/Experiments/hello_world.c)
add_executable(CS223_repository_spring_2017 ${SOURCE_FILES})