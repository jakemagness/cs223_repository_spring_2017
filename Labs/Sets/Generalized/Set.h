/** Set.h
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section:
 * Project: Set Data Structure
 * Purpose: Implement an abstract data type for a "set"
 * ===========================================================
 */

#include <stdlib.h>
#include "HashTable.h"

#ifndef SET_H
#define SET_H

typedef int Boolean;

#define FALSE                   0
#define TRUE                    1
#define ILLEGAL_ELEMENT        -1

// A Universal Set - a set of values in a hash table
typedef HashTable UniversalSet;

typedef struct set {
    UniversalSet * u;   // The Universal Set that defines the set's values
    size_t  bitsSize;   // How many integers are used to store the set bits
    int *   bits;       // An array of integers for the bits (one bit per element; 32 bits per int)
    size_t  bitsPerInt; // The number of bits per integer
} Set;

UniversalSet * universalSetCreate(unsigned int expectedNumberOfElements,
                                  hashFunction hashFunc,
                                  compareFunction compareFunc);
void           universalSetDelete(UniversalSet * u, Boolean deleteElements);
void           universalSetInsert(UniversalSet * u, void * newElement);
int            universalSetElementIndex(UniversalSet * u, void * element);

Set * setCreate(UniversalSet * u);
void  setDelete(Set * set);

void  setAddElement   (Set * set, void * element);
void  setDeleteElement(Set * set, void * element);

Set * setUnion       (Set * s1, Set * s2);
Set * setIntersection(Set * s1, Set * s2);
Set * setDifference  (Set * s1, Set * s2);
Set * setComplement  (Set * s1);

Boolean setIsIn          (Set * set, void * element);
Boolean setEquals        (Set * s1, Set * s2);
Boolean setNotEquals     (Set * s1, Set * s2);
Boolean setSubset        (Set * s1, Set * s2);
Boolean setProperSubset  (Set * s1, Set * s2);
Boolean setSuperset      (Set * s1, Set * s2);
Boolean setProperSuperset(Set * s1, Set * s2);

void setPrint(Set *s, char *description);
int  setCardinality(Set *s);

#endif // SET_H
