/** Set.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section:
 * Project: Set Data Structure
 * Purpose: Implement an abstract data type for a "set"
 * ===========================================================
 */

#include <stdio.h>
#include "Set.h"

//---------------------------------------------------------------------
UniversalSet * universalSetCreate(unsigned int expectedNumberOfElements,
                                  hashFunction hashFunc,
                                  compareFunction compareFunc) {
    return hashTableCreate(expectedNumberOfElements*2, hashFunc, compareFunc);
}

//---------------------------------------------------------------------
void universalSetDelete(UniversalSet *u, Boolean deleteElements) {
    hashTableDelete(u, deleteElements);
}

//---------------------------------------------------------------------
void universalSetInsert(UniversalSet * u, void * newElement) {
    hashTableInsertElement(u, newElement);
}

//---------------------------------------------------------------------
int universalSetElementIndex(UniversalSet * u, void * element) {
    return hashTableFind(u, element);
}

//---------------------------------------------------------------------
Set * setCreate(UniversalSet * u) {
    Set * set = (Set *) malloc(sizeof(Set));

    int maxPossibleElements = u->arraySize;

    // How many integers do we need to store the bit flags?
    size_t bitsPerInt = sizeof(unsigned int) * 8;
    size_t n = maxPossibleElements / bitsPerInt;
    if (maxPossibleElements % bitsPerInt != 0) {
        n++; // round the number of integers up
    }

    set->u = u;
    set->bitsSize = n;
    set->bits = (int *) calloc(n, sizeof(int) );
    set->bitsPerInt = bitsPerInt;

    return set;
}

//---------------------------------------------------------------------
void  setDelete(Set *set) {
    free(set->bits);
    free(set);
}

//---------------------------------------------------------------------
void  setAddElement(Set * set, void * element) {
    int index = universalSetElementIndex(set->u, element);
    if (index != ILLEGAL_ELEMENT) {
        size_t whichInt = index / set->bitsPerInt;
        size_t whichBit = index % set->bitsPerInt;
        set->bits[whichInt] = set->bits[whichInt] | (1 << whichBit);
    }
}

//---------------------------------------------------------------------
void  setDeleteElement(Set * set, void * element) {
    int index = universalSetElementIndex(set->u, element);
    if (index != ILLEGAL_ELEMENT) {
        size_t whichInt = index / set->bitsPerInt;
        size_t whichBit = index % set->bitsPerInt;
        set->bits[whichInt] = set->bits[whichInt] & ~(1 << whichBit);
    }
}

//---------------------------------------------------------------------
Boolean setIsIn(Set * set, void * element) {
    int index = universalSetElementIndex(set->u, element);
    if (index != ILLEGAL_ELEMENT) {
        size_t whichInt = index / set->bitsPerInt;
        size_t whichBit = index % set->bitsPerInt;
        return (set->bits[whichInt] & (1 << whichBit)) > 0;
    }
    return FALSE;
}

//---------------------------------------------------------------------
static void verifySetsHaveSameUniverse(Set *s1, Set *s2) {
    if (s1->u != s2->u) {
        printf("Error: sets do not have common elements. Exiting... \n");
        exit(1);
    }
}

//---------------------------------------------------------------------
Set * setUnion(Set * s1, Set * s2) {
    verifySetsHaveSameUniverse(s1, s2);

    // Create a new set that will hold the union of s1 and s2
    Set * newSet = setCreate(s1->u);

    // Combine all of the bits from both sets.
    for (int j=0; j<s1->bitsSize; j++) {
        newSet->bits[j] = s1->bits[j] | s2->bits[j];
    }

    return newSet;
}

//---------------------------------------------------------------------
Set * setIntersection(Set * s1, Set * s2) {
    verifySetsHaveSameUniverse(s1, s2);

    // Create a new set that will hold the intersection of s1 and s2
    Set * newSet = setCreate(s1->u);

    // Only set bits if they are set in both s1 and s2
    for (int j=0; j<s1->bitsSize; j++) {
        newSet->bits[j] = s1->bits[j] & s2->bits[j];
    }

    return newSet;
}

//---------------------------------------------------------------------
Set * setDifference(Set * s1, Set * s2) {
    verifySetsHaveSameUniverse(s1, s2);

    // Create a new set that will hold the difference of s1 and s2
    Set * newSet = setCreate(s1->u);

    // Remove any bits that are in s2 and also in s1
    for (int j=0; j<s1->bitsSize; j++) {
        newSet->bits[j] = s1->bits[j] & ~(s2->bits[j]);
    }

    return newSet;
}

//---------------------------------------------------------------------
Set * setComplement(Set * s1) {

    // Create a new set that holds the complement
    Set * newSet = setCreate(s1->u);

    // Remove any bits that are in s2 and also in s1
    for (int j=0; j<s1->bitsSize; j++) {
        newSet->bits[j] = ~(s1->bits[j]);
    }

    return newSet;
}

//---------------------------------------------------------------------
Boolean setEquals(Set * s1, Set * s2) {

    // To be equal, every integer in the bits array must be equal.
    for (int j=0; j<s1->bitsSize; j++) {
        if (s1->bits[j] != s2->bits[j]) {
            return FALSE;
        };
    }

    return TRUE;
}

//---------------------------------------------------------------------
Boolean setNotEquals(Set * s1, Set * s2) {
    return ! setEquals(s1, s2);
}

//---------------------------------------------------------------------
Boolean setSubset(Set * s1, Set * s2) {
    // To be a subset, remove every element of s2 from s1 and test that s1 is empty
    for (int j=0; j<s1->bitsSize; j++) {
        if (s1->bits[j] & ~(s2->bits[j])) {
            return FALSE;
        }
    }

    return TRUE;
}

//---------------------------------------------------------------------
Boolean setProperSubset(Set * s1, Set * s2) {
    return setSubset(s1,s2) & setNotEquals(s1, s2);
}

//---------------------------------------------------------------------
Boolean setSuperset(Set * s1, Set * s2) {
    // s1 is a superset of s2 if s2 is a subset of s1
    return setSubset(s2, s1);
}

//---------------------------------------------------------------------
Boolean setProperSuperset(Set * s1, Set * s2) {
    return setSubset(s2, s1) & setNotEquals(s1, s2);
}

//---------------------------------------------------------------------
void setPrint(Set *s, char *description) {
    printf("Set %s: ", description);
    for (int j = 0; j < s->u->arraySize; j++) {
        if (s->u->array[j] != NULL) {
            size_t whichInt = j / s->bitsPerInt;
            size_t whichBit = j % s->bitsPerInt;
            if (s->bits[whichInt] & (1 << whichBit)) {
                printf("%s ", (char *) s->u->array[j]); // Assumes string data in hash table
            }
        }
    }
    printf("\n");
}

//---------------------------------------------------------------------
int  setCardinality(Set *s) {
    int cardinality = 0;
    for (int j = 0; j < s->u->arraySize; j++) {
        if (s->u->array[j] != NULL) {
            size_t whichInt = j / s->bitsPerInt;
            size_t whichBit = j % s->bitsPerInt;
            if (s->bits[whichInt] & (1 << whichBit)) {
                cardinality++;
            }
        }
    }
   return cardinality;
}

