/** HashTable.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section:
 * Project: Set Data Structure
 * Purpose: Implement an abstract data type for a "set"
 * ===========================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "HashTable.h"

/** -------------------------------------------------------------------
 * Creates a new, empty hash table.
 * @param tableSize - the size of the hash table. It should
 *                    typically be at least twice as large as
 *                    the number of values you expect to store
 *                    in the hash table.
 * @param hashFunc - a pointer to a function that can calculate an
 *               array index from specific data in a HashTableElement.
 * @param compareFunc - a pointer to a function that can compare two
 *               HashTableElements for equality.
 * @return a pointer to a new hash table
 */
HashTable *hashTableCreate(unsigned int tableSize,
                           hashFunction hashFunc,
                           compareFunction compareFunc) {

    // Make the table size always be a power of 2.
    double exponent = ceil(log2(tableSize));
    tableSize = (unsigned int) pow(2.0, exponent);

    HashTable *newTable = (HashTable *) malloc(sizeof(HashTable));

    newTable->keyToIndex      = hashFunc;
    newTable->compareElements = compareFunc;
    newTable->numberElements  = 0;
    newTable->arraySize       = tableSize;
    newTable->array = (void **) calloc(tableSize, sizeof(void *));

    return newTable;
}

/** -------------------------------------------------------------------
 * Delete a hash table.
 * @param table - the table to be deleted
 * @param deleteElements - If TRUE, the elements that the hash table contains
 *                         are deleted. If FALSE, the elements in the hash
 *                         table are not deleted, but the hash table pointers
 *                         are all deleted.
 */
void hashTableDelete(HashTable* table, Boolean deleteElements) {
    if (deleteElements) {
        for (int j=0; j<table->arraySize; j++) {
            if (table->array[j]) {
                free(table->array[j]);
            }
        }
    }
    free(table->array);
    free(table);
}

/** -------------------------------------------------------------------
 * Insert a new element into a hash table.
 * @param table - the hash table to be changed.
 * @param newElement - the element to be inserted
 * @return - return the position in the array where the element was stored.
 */
int hashTableInsertElement(HashTable* table, void * newElement) {
    // Calculate the location where the element should be.
    int index = table->keyToIndex(table, newElement);

    // Use linear probing to find an empty slot starting at index
    int count = 0;
    while (table->array[index] != NULL && count < table->arraySize) {
        // Make sure the element is not already in the table
        if (table->compareElements(table->array[index], newElement)) {
            // The element is already in the table, so don't insert it.
            return index;
        }
        index++;
        count++;
    }

    if (count < table->arraySize && table->array[index] == NULL) {
        table->array[index] = newElement;
        table->numberElements++;
        return index;
    } else {
        printf("Error in hashTableInsertElement; the element could not be inserted.\n");
        exit(1);
    }
}

/** -------------------------------------------------------------------
 * Find an element in a hash table.
 * @param table - the hash table.
 * @param toFind - the element to search for.
 * @return - if the element is found, return a pointer to element,
 *           otherwise return NULL to indicate that the element was
 *           not found.
 */
int hashTableFind(HashTable* table, void * toFind) {
    // Calculate the location where the element should be.
    int index = table->keyToIndex(table, toFind);

    // Use linear probing to find the element
    int count = 0;
    while (table->array[index] != NULL && count < table->arraySize) {
        // Try this element
        if (table->compareElements(table->array[index], toFind)) {
            // Found the element
            return index;
        }
        index++;
        count++;
    }
    return -1; // The element was not found
}



