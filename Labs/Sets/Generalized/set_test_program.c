//
// Created by Wayne.Brown on 5/1/2017.
//

#include <stdio.h>
#include <string.h>
#include "Set.h"

//---------------------------------------------------------------------
unsigned int hashFunc(HashTable *table, void *word) {
    char * w = (char *) word;
    unsigned int total = 0;
    for (int j=0; w[j] != '\0'; j++) {
        total += (total << 8) + (unsigned int) w[j];
    }
    total += strlen(w);
    return (unsigned int) (total % table->arraySize);
}

//---------------------------------------------------------------------
Boolean compareTwoStrings(void * e1, void * e2) {
    char * s1 = (char *) e1;
    char * s2 = (char *) e2;
    return strcmp(s1,s2) == 0;
}

//---------------------------------------------------------------------
int main() {
    char *names[] = {"Bob", "Alice", "Sam", "Mary", "Fred",
                     "Sue", "Amy", "James", "Micheal", "Sandy"};
    unsigned int expectedNumberOfElements = sizeof(names) / sizeof(char *);
    UniversalSet * x = universalSetCreate(expectedNumberOfElements,
                                          hashFunc, compareTwoStrings);
    // Add all of the possible values to the universal set
    for (int j=0; j<expectedNumberOfElements; j++) {
        universalSetInsert(x, names[j]);
    }

    Set * a = setCreate(x);
    setAddElement(a, "Bob");
    setAddElement(a, "Alice");
    setAddElement(a, "Sam");

    Set * b = setCreate(x);
    setAddElement(b, "Bob");
    setAddElement(b, "Alice");
    setAddElement(b, "Mary");

    Set * c = setUnion(a, b);
    printf("\n");
    setPrint(a, "a");
    setPrint(b, "b");
    setPrint(c, "a union b");
    setDelete(c);

    setAddElement(a, "James");
    c = setUnion(a, b);
    printf("\n");
    setPrint(a, "a");
    setPrint(b, "b");
    setPrint(c, "a union b");
    setDelete(c);

    setDeleteElement(a, "Alice");
    c = setUnion(a, b);
    printf("\n");
    setPrint(a, "a");
    setPrint(b, "b");
    setPrint(c, "a union b");
    setDelete(c);

    c = setIntersection(a, b);
    printf("\n");
    setPrint(c, "a intersect b");
    setDelete(c);

    c = setDifference(a, b);
    printf("\n");
    setPrint(a, "a");
    setPrint(b, "b");
    setPrint(c, "a minus b");
    setDelete(c);

    c = setDifference(b, a);
    printf("\n");
    setPrint(a, "a");
    setPrint(b, "b");
    setPrint(c, "b minus a");
    setDelete(c);

    c = setComplement(a);
    printf("\n");
    setPrint(a, "a");
    setPrint(c, "~a");
    setDelete(c);

    c = setComplement(b);
    printf("\n");
    setPrint(b, "b");
    setPrint(c, "~b");
    setDelete(c);

    printf("\n");
    printf("a equals b %s\n", (setEquals(a,b) ? "TRUE" : "FALSE"));
    printf("a not equals b %s\n", (setNotEquals(a,b) ? "TRUE" : "FALSE"));
    printf("a subset b %s\n", (setSubset(a,b) ? "TRUE" : "FALSE"));
    printf("a superset b %s\n", (setSuperset(a,b) ? "TRUE" : "FALSE"));

    Set * d = setCreate(x);
    setAddElement(d, "Mary");
    setAddElement(d, "Sam");

    Set * e = setCreate(x);
    setAddElement(e, "Mary");
    setAddElement(e, "Sam");

    printf("\n");
    printf("d equals e %s\n", (setEquals(d,e) ? "TRUE" : "FALSE"));
    printf("d not equals e %s\n", (setNotEquals(d,e) ? "TRUE" : "FALSE"));
    printf("d subset e %s\n", (setSubset(d,e) ? "TRUE" : "FALSE"));
    printf("d superset e %s\n", (setSuperset(d,e) ? "TRUE" : "FALSE"));

}