//
// Created by C18Jake.Magness on 2/2/2017.
//

#include "stack.h"

Stack *     stackCreate (){
    return arrayListCreate(10);
}

void        stackDelete (Stack *s){
    arrayListDelete(s);
}

void        stackPush   (Stack *s, ElementType element){
    arrayListAppend(s, &element);
}

ElementType stackPop    (Stack *s){
    ElementType value = arrayListGetElement(s, s->number_elements-1);
    s->number_elements--;
    return value;
}

int         stackIsEmpty(Stack *s){
    if(arrayListSize(s) == 0){
        return 1;
    }
    else{
        return 0;
    }
}

ElementType stackPeek   (Stack *s, int index);
ElementType stackTop    (Stack *s);
int         stackSize   (Stack *s);
void        stackClear  (Stack *s);