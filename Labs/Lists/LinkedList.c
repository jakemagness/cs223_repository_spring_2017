/** LinkedList.c
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a list using an array.
 * Purpose: General purpose list implementation.
 * Doc: Basis of code was provided by Dr. Brown.  Used textbook
 * to build selection sort algorithm. Dr. Brown helped me fix
 * the selection sort code.
 * ===========================================================
 */

#include "LinkedList.h"
#include <stdio.h>
#include <stdlib.h>

/** ----------------------------------------------------------
 * Create a new linked list.
 * @return The address of a new linked list
 */
LinkedList * linkedListCreate() {
    LinkedList * my_list = NULL;

    my_list = (LinkedList *) malloc(sizeof(LinkedList));
    my_list->first = NULL;
    my_list->last = NULL;
    my_list->numberElements = 0;

    return my_list;
}

/** ----------------------------------------------------------
 * Delete a linked list.
 * @param list
 */
void linkedListDelete(LinkedList * list) {
    // delete all of the nodes in the list
    Node * nodePointer = list->first;
    Node * nextNode = NULL;

    while (nodePointer != NULL) {
        nextNode = nodePointer->next;
        free(nodePointer);
        nodePointer = nextNode;
    }

    // This is not necessary, because the list should not
    // be accessed any more, but this makes sure that if
    // it was, the list would be empty.
    list->first = NULL;
    list->last = NULL;
    list->numberElements = 0;

    // Delete the meta-data for the list.
    free(list);
}

/** ----------------------------------------------------------
 * Append an new element on the end of the list.
 * @param list - the linked list
 * @param element - the new element to be added to the list.
 */
void linkedListAppend(LinkedList * list, ElementType * element) {
    Node * node;

    node = (Node*) malloc(sizeof(Node));

    node->data = *element;
    node->next = NULL;

    if (list->last != NULL){
        list->last->next = node;
        list->last = node;
    }
    else{
        list->first = node;
        list->last = node;
    }
    list->numberElements++;
}

/** ----------------------------------------------------------
 * Insert a new element into a linked list at the desired position
 * @param list - the linked list
 * @param position - the position in the list where the new element is to be added.
 * @param element - the new element for the list
 */
void linkedListInsertElement(LinkedList *list, int position, ElementType value) {
    Node * address = list->first;
    Node * reassign = list->first->next;
    Node * node;
    node = (Node*) malloc(sizeof(Node));
    node->data = value;
    node->next = NULL;

    if(position != 0) {
        for(int i = 0; i < position; i++){
            reassign = address;
            address = address->next;
        }
        reassign->next = node;
        node->next = address;
    }
    else{
        node->next = list->first;
        list->first = node;
    }
}

/** ----------------------------------------------------------
 * Retrieve the element at the desired position.
 * @param list - the linked list
 * @param position - which position in the list to retrieve
 * @return a single element from the list from the desired position
 */
ElementType linkedListGetElement(LinkedList * list, int position) {
    Node * address = list->first;
    ElementType data = list->first->data;

    if(position!=0){
        for (int i = 0; i < position; i++){
            address = address->next;
            data = address->data;
        }
    }

    return data;
}
/** ----------------------------------------------------------
 * Delete an element from a linked list at a specific position
 * @param list - the linked list
 * @param position - the position in the array of the element to delete.
 */
void linkedListDeleteElement(LinkedList * list, int position) {
    Node * address = list->first;
    Node * connect = list->first;
    Node * reassign = list->first->next;

    if(position != 0) {
        for(int i = 0; i < position; i++){
            reassign = address;
            address = address->next;
        }
        reassign->next = address->next;
        free(address);
    }
    else{
        reassign = list->first;
        list->first->data = list->first->next->data;
        list->first = list->first->next;
        free(reassign);
    }
}
/** ----------------------------------------------------------
 * Print every element in the linked list.
 * @param list - the linked list
 */
void linkedListPrint(LinkedList * list) {
    Node * address;
    address = list->first;
    for(int i = 0; i < list->numberElements; i++){
        //Go through to the next list over and over
        printf("%d ", address->data);
        address = address->next;
    }
}

/** ----------------------------------------------------------
 * Perform a selection sort on a linked list struct.
 * @param list - the linked list
 */
void linkedListSelectionSort(LinkedList * a){
    if(a->numberElements>0){
        Node * temp = a->first;
        Node * temp2 = a->first->next;
        ElementType data;
        for(int i = 0; i < a->numberElements-2; i++){
            for(int j = 0; j < a->numberElements-1-i; j++ ){
                if(temp2->data < temp->data){
                    data = temp->data;
                    temp->data = temp2->data;
                    temp2->data = data;
                }
                temp2 = temp2->next;
            }
            temp = temp->next;
            temp2 = temp->next;
        }
    }

}

/** ----------------------------------------------------------
 * Change an element at a certain position of a linked list.
 * @param list - the linked list
 * @param position - the position in the array of the element to change.
 * @param newValue - the value of the element.
 */
void linkedListChangeElement(LinkedList *list, int position, ElementType newValue){
    Node * address = list->first;
    ElementType data = list->first->data;
    if(position!=0){
        for (int i = 0; i < position; i++){
            address = address->next;
        }
    }
    address->data = newValue;
}

/** ----------------------------------------------------------
 * Find an element at a certain position of a linked list.
 * @param list - the linked list
 * @param value - the value of the data to search for.
 */
int linkedListFindElement(LinkedList *list, ElementType value){
    Node * address = list->first;
    ElementType data = list->first->data;
    int index = -1;

    for(int i = 0; i < list->numberElements-1; i++) {
        if (data == value) {
            return i;
        }
        else if (list->last->data == value){
            return list->numberElements-1;
        }
        address = address->next;
        data = address->data;
    }
    return index;
}