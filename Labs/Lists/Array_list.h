//
// Created by C18Jake.Magness on 1/10/2017.
//

#ifndef CS223_REPOSITORY_SPRING_2017_ARRAY_LIST_H
#define CS223_REPOSITORY_SPRING_2017_ARRAY_LIST_H

typedef int Element_type;

typedef struct Array_list {
    int array_size;
    int number_of_element;
    Element_type *list;
}Array_list;

int array_length(Array_list *my_list);
Array_list *create_array_list(int size);
void delete_array_list(Array_list *my_list);
void append_array_list(Array_list *my_list, Element_type *element);

#endif //CS223_REPOSITORY_SPRING_2017_ARRAY_LIST_H
