/** linkedListTestProgram.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section: M3A
 * Project: Test the functionality of a linked list.
 * Purpose: Software verification. (Does the software work correctly?)
 * ===========================================================
 */
#include "LinkedList.h"
#include <stdio.h>

int main() {
    LinkedList * a;

    a = linkedListCreate();
    for (int element = 20; element < 37; element++) {
        linkedListAppend(a, &element);
//        linkedListPrint(a);
    }


//    // Delete two elements
    linkedListDeleteElement(a, 5);
    linkedListPrint(a);
    linkedListDeleteElement(a, 10);
//    linkedListPrint(a);
//
//    // Get two elements
//    printf("Element at position 0 is %d\n", linkedListGetElement(a, 0));
//    printf("Element at position 10 is %d\n", linkedListGetElement(a, 10));
//
//    // Generate an error
//    printf("Element at position 100 is %d\n", linkedListGetElement(a, 100));
//
//    linkedListDelete(a);
}