//
// Created by C18Jake.Magness on 1/10/2017.
//

#include "Array_list.h"
#include <stdlib.h>

int array_length(Array_list *my_list){
    return my_list->array_size;
}

//Function that returns a pointer of type array_list
Array_list *create_array_list(int size){
    Array_list *my_list;

    my_list = (Array_list *)malloc(sizeof(Array_list)); //Determines the size of the Array_list struct.  The pointer is
                                                        // saved under pointer variable my_list.
    my_list->array_size = size;
    my_list->number_of_element = 0;                     //Accessing the struct using the pointer.
    my_list->list = (Element_type *) malloc(sizeof(Element_type) * size);  //Cast because Malloc doesn't return datatype

    return my_list;
}

void delete_array_list(Array_list *my_list){

    free(my_list->list); //Free up the list's memory location
    free(my_list);       //Free up my_list memory location

}

void append_array_list(Array_list *my_list, Element_type *element){

    if(my_list->number_of_element < my_list->array_size)
    {
        my_list->list[my_list->number_of_element] = *element;
        my_list->number_of_element += 1;
    }
    //I think the below code works to append outside range of array.
    else
    {
        new_list = (Array_list *)malloc(sizeof(my_list->array_size)); // Create new struct of the correct size
        int i = 0;
        while(i < my_list->array_size) //Copies data into the new, larger array
        {
            new_list->list[i] = my_list->list[i];
            i++;
        }
        new_list->list[i] = *element;
        new_list[i+1] = '\0';
        my_list = new_list; //Reassign Pointer values
    }
}
