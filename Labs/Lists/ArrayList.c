/** ArrayList.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section: M3A
 * Project: Implement a list using an array.
 * Purpose: General purpose list implementation.
 * ===========================================================
 */

#include <stdio.h>
#include <stdlib.h>  // Defines malloc and free

#include "ArrayList.h"

/** ----------------------------------------------------------
 * Create a new list.
 * @param size - the initial size of the array that will hold
 *               the list elements.
 * @return A pointer to a newly allocated and initialized
 *         ArrayList structure.
 */
ArrayList *arrayListCreate(int initial_size) {
    ArrayList *my_list;

    // Create the structure that holds the list information
    my_list = (ArrayList *) malloc(sizeof(ArrayList));

    // Set the fields of the ArrayList
    // Allocate a block of memory to hold the elements
    my_list->array = (ElementType *) malloc(sizeof(ElementType) * initial_size);
    my_list->array_size = initial_size;
    my_list->number_elements = 0;

    return my_list;
}
/** ----------------------------------------------------------
 * Delete a list.
 * @param list - a pointer to the list to be delete.
 */
void arrayListDelete(ArrayList *list) {
    // Delete the array that holds the elements of the list
    free(list->array);

    // Delete the structure that holds the list information
    free(list);
}

/** ----------------------------------------------------------
 * Append a new element onto the end of a list.
 * @param list - a pointer to the list to be manipulated.
 * @param element - a pointer to the element to to be added.
 */
void arrayListAppend(ArrayList *list, ElementType *element) {
    // Make sure there is enough room in the list for the new element
    if (list->number_elements >= list->array_size) {
        // Create a bigger array
        int new_size = list->array_size + 5;
        ElementType *new_array = (ElementType *) malloc(sizeof(ElementType) * new_size);

        // Copy the data from the original array into this new array
        for (int j=0; j < list->number_elements; j++) {
            new_array[j] = list->array[j];
        }

        free(list->array);
        list->array = new_array;
        list->array_size = new_size;
    }

    list->array[list->number_elements] = *element;
    list->number_elements += 1;
}

/** ----------------------------------------------------------
 * Print the elements in a list in sequential order.
 * @param list - a pointer to the list to be printed.
 */
void arrayListPrint(ArrayList *list) {
    printf("list array size = %d\n", list->array_size);
    printf("list number of elements = %d\n", list->number_elements);
    for (int j=0; j<list->number_elements; j++) {
        printf("[%d] = %d\n", j, list->array[j]);
    }
}

/** ----------------------------------------------------------
 * Retrieve one element from a list.
 * @param list - a pointer to the list.
 * @param index - the position in the list to retrieve.
 * @return A single element from the list.
 */
ElementType arrayListGetElement(ArrayList *list, int index) {
    if (index >= 0 && index < list->number_elements) {
        return list->array[index];
    } else {
        printf("arrayListGetElement failed to get element %d.", index);
        printf("The list only contains %d elements.", list->number_elements);
        exit(1);
    }
}

/** ----------------------------------------------------------
 * Delete an element from a list at the index position.
 * @param list - a pointer to the list to be manipulated.
 * @param index - the position to be deleted.
 */
void arrayListDeleteElement(ArrayList *list, int index) {
    if (index >= 0 && index < list->number_elements) {
        // Move all the elements past the one to delete to be deleted.
        for (int j=index; j < list->number_elements; j++) {
            list->array[j] = list->array[j+1];
        }
        list->number_elements--;
    } else {
        printf("Tried to delete the %d element of a list, but only %d elements exist. Exiting.",
               index, list->number_elements);
        exit(1);
    }
}

/** ----------------------------------------------------------
 * Find the position of an element in a list.
 * @param llst - a pointer to the list.
 * @param value - the value to search for.
 * @return The first position in the list where the value was found, or
 *         -1 is the value was not found.
 */
int arrayListFindElement(ArrayList *list, ElementType value) {
    for (int j=0; j < list->number_elements; j++) {
        if (list->array[j] == value) {
            return j;
        }
    }
    // The value was not found
    return -1;
}

void arrayListSelectionSort(ArrayList *list){

    ElementType lowest = 0;
    int location = 0;
    ElementType temp = 0;
    int length = list->array_size;

    //Changed i < list->array_size-1 to just array_size
    for(int i = 0; i < list->array_size; i++){
        lowest = list->array[i];

        for(int j = 0; j < list->array_size; j++){
            if(list->array[j] < lowest){
                lowest = list->array[j];
                temp = list->array[i];
                list->array[i] = list->array[j];
                list->array[j] = temp;
                printf("New temp = %d\n", list->array[1]);
            }
        }
        printf("New lowest = %d\n", lowest);
    }
}

void arrayListInsertionSort(ArrayList *list){

}

void arrayListChangeElement(ArrayList *list, int position, ElementType newValue){
    list->array[position] = newValue;
}
void arrayListInsertElement(ArrayList *list, int position, ElementType value){
    //Create an array of greater size.
    int new_size = list->array_size + 1;
    ElementType *new_array = (ElementType *) malloc(sizeof(ElementType) * new_size);
    ElementType temp_value = 0;
    list->number_elements++;

    for (int j = 0; j < list->number_elements; j++) {
        if(position == j){
            new_array[j] = value;
            new_array[j+1] = list->array[j];
        }
        else if (j > position){
            new_array[j+1] = list->array[j];
        }
        else if(j < position){
            new_array[j] = list->array[j];
        }
    }
    list->array = new_array;
}

int arrayListSize(ArrayList *list){
    return list->number_elements;
}