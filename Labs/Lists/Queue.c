//
// Created by C18Jake.Magness on 2/9/2017.
//

#include "Queue.h"

Queue * queueCreate (){
    return linkedListCreate();
}

void    queueDelete (Queue *q){
    linkedListDelete(q);
}

void    queueEnqueue(Queue *q, ElementType element)
{
    linkedListAppend(q, &element);
}

ElementType queueDequeue(Queue *q){
    ElementType temp = linkedListGetElement(q, 0);
    linkedListDeleteElement(q, 0);
    return temp;
}

int         queueIsEmpty(Queue *q){
    if(q->numberElements == 0){
        return 1;
    }
    else{
        return 0;
    }
}

ElementType queuePeek   (Queue *q, int index){
    return linkedListGetElement(q, index);
}

ElementType queueFront  (Queue *q){
    return linkedListGetElement(q, 0);
}

ElementType queueBack   (Queue *q){
    return linkedListGetElement(q, q->numberElements-1);
}

int         queueSize   (Queue *q){
    return q->numberElements;
}

void        queueClear  (Queue *q){

}
