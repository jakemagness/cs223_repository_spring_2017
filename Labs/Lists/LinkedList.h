/** LinkedList.h
 * ===========================================================
 * Name: C2C Jake Magness, Spring 2017
 * Section: M3A
 * Project: Implement a Linked List using.
 * Purpose: General purpose list implementation.
 * Doc: Basis of code was provided by Dr. Brown.
 * ===========================================================
 */

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

// Define the data type for the elements that will be stored in this linked list.
// This definition simply makes it easier to change the type of data the list stores.
typedef int ElementType;

// Define on node of the linked list
typedef struct node {
    ElementType   data;
    struct node * next;
} Node;

// Define the meta-data that stores the linked list.
typedef struct linkedList {
    Node * first;
    Node * last;
    int    numberElements;
} LinkedList;

// Functions that manipulate a linked list
LinkedList * linkedListCreate();
void         linkedListDelete(LinkedList * list);
void         linkedListAppend(LinkedList *list, ElementType *element);
void         linkedListPrint(LinkedList *list);
ElementType  linkedListGetElement(LinkedList *list, int position);
void         linkedListDeleteElement(LinkedList *list, int position);
void         linkedListInsertElement(LinkedList *list, int position, ElementType value);
void         linkedListChangeElement(LinkedList *list, int position, ElementType newValue);
int          linkedListFindElement(LinkedList *list, ElementType value);
void         linkedListSelectionSort(LinkedList *list);
void         swap(Node * first, Node * second);

#endif // LINKED_LIST_H
