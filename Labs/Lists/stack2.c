//
// Created by C18Jake.Magness on 2/2/2017.
//

#include "stack.h"
#include <stdio.h>
#include <stdlib.h>  // Defines malloc and free

Stack *     stackCreate (){
    ArrayList *my_list;
    ElementType initial_size = 10;

    // Create the structure that holds the list information
    my_list = (ArrayList *) malloc(sizeof(ArrayList));

    // Set the fields of the ArrayList
    // Allocate a block of memory to hold the elements
    my_list->array = (ElementType *) malloc(sizeof(ElementType) * initial_size);
    my_list->array_size = initial_size;
    my_list->number_elements = 0;

    return my_list;
}

void        stackDelete (Stack *s){
    free(s->array);
    free(s);
}

void        stackPush   (Stack *s, ElementType element){
    // Make sure there is enough room in the list for the new element
    if (s->number_elements >= s->array_size) {
        // Create a bigger array
        int new_size = s->array_size + 5;
        ElementType *new_array = (ElementType *) malloc(sizeof(ElementType) * new_size);

        // Copy the data from the original array into this new array
        for (int j=0; j < s->number_elements; j++) {
            new_array[j] = s->array[j];
        }

        free(s->array);
        s->array = new_array;
        s->array_size = new_size;
    }

    s->array[s->number_elements] = element;
    s->number_elements += 1;
}

ElementType stackPop    (Stack *s){
    if (s->number_elements >= 0) {
        ElementType value = s->array[s->number_elements-1];
        s->number_elements--;
        return value;
    } else {
        exit(1);
    }
}

int         stackIsEmpty(Stack *s){
    if(s->number_elements == 0){
        return 1;
    }
    else{
        return 0;
    }
}

ElementType stackPeek   (Stack *s, int index);
ElementType stackTop    (Stack *s);
int         stackSize   (Stack *s);
void        stackClear  (Stack *s);