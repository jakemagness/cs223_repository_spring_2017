/** stack.h
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section: M3A
 * Project: Lab 11
 * Purpose: Define an interface to a stack data structure.
 * ===========================================================
 */

#ifndef STACK_H
#define STACK_H

#include "ArrayList.h"

typedef ArrayList Stack;

// Standard stack functionality
Stack *     stackCreate ();
void        stackDelete (Stack *s);
void        stackPush   (Stack *s, ElementType element);
ElementType stackPop    (Stack *s);
int         stackIsEmpty(Stack *s);

// Optional stack functionality
ElementType stackPeek   (Stack *s, int index);
ElementType stackTop    (Stack *s);
int         stackSize   (Stack *s);
void        stackClear  (Stack *s);

#endif // STACK_H
