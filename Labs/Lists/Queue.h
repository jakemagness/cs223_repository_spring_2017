/** Queue.h
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section: M3A
 * Project: Lab 13
 * Purpose: Define an interface to a Queue data structure.
 * ===========================================================
 */

#ifndef QUEUE_H
#define QUEUE_H

#include "LinkedList.h"  // FIX THIS to include the correct path to your LinkedList.h file

typedef LinkedList Queue;

// Standard Queue functionality
Queue *     queueCreate ();
void        queueDelete (Queue *q);
void        queueEnqueue(Queue *q, ElementType element);
ElementType queueDequeue(Queue *q);
int         queueIsEmpty(Queue *q);

// Optional Queue functionality
ElementType queuePeek   (Queue *q, int index);
ElementType queueFront  (Queue *q);
ElementType queueBack   (Queue *q);
int         queueSize   (Queue *q);
void        queueClear  (Queue *q);

#endif // QUEUE_H
