/** heapSort.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section:
 * Project: Lab 30
 * Purpose: Implement a heapSort
 * ===========================================================
 */

#include <stdio.h>
#include "heap.h"

Boolean compareInts(void * n, void * m);
void printIntArray(int array[], int size);
void convertToMaxHeap(int array[], int n);
void heapSort(int[], int);
void siftDown(int[], int, int);

int main() {
    int num[] = {0, 37, 25, 43, 65, 48, 84, 73, 18, 79, 56, 69, 32};
    int n = 12;

    Heap * integerHeap = maxHeapCreate(num, n+1, n, sizeof(int),compareInts);
    printIntArray(num, n);

} //end main

/** -------------------------------------------------------------------
 * Compare two integers.
 * @param n - the address of the first integer
 * @param m - the address of the second integer
 * @return TRUE if the first integer is greater than the second integer
 */
Boolean compareInts(void * n, void * m) {
    int * a = (int *) n;
    int * b = (int *) m;
    return *a > *b;
}

/** -------------------------------------------------------------------
 * Print the contents of an array on a single line.
 * @param array - the array to print
 * @param size - the index of the last value in the array;
 *               the array contents are in positions [1] to [last]
 */
void printIntArray(int array[], int size) {
    for (int j = 0; j <= size; j++) {
        printf("%3d ", array[j]);
    }
    printf("\n");
}
