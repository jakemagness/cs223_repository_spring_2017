//
// Created by Wayne.Brown on 3/6/2017.
//

#include <stdlib.h>
#include <stdio.h>
#include "AvlTree.h"

//---------------------------------------------------------------------
// Helper functions
void          avlUpdateHeight(AvlTreeNode * tree);
AvlTreeNode * avlInsertRecursive(AvlTreeNode * tree, AvlTreeNode * newNode);
void          avlPrintRecursive(AvlTreeNode * tree);
AvlTreeNode * avlRebalance(AvlTreeNode * tree);

//---------------------------------------------------------------------
AvlTreeNode * avlCreateTree() {
    return NULL;
}

//---------------------------------------------------------------------
AvlTreeNode * avlDeleteTree(AvlTreeNode * tree) {
    if (tree != NULL) {
        avlDeleteTree(tree->leftTree);
        avlDeleteTree(tree->rightTree);
        free(tree);
    }
    return NULL;
}

//---------------------------------------------------------------------
AvlTreeNode * avlInsert(AvlTreeNode * tree, ElementType newValue) {
    // Create a new node
    AvlTreeNode * newNode = (AvlTreeNode *) malloc(sizeof(AvlTreeNode));
    if (newNode == NULL) {
        printf("Error in avlInsert. No more memory!");
        exit(1);
    }

    // Initialize the newNode's values
    newNode->data = newValue;
    newNode->leftTree = NULL;
    newNode->rightTree = NULL;
    newNode->height = 1;
    newNode->heightDifference = 0;

    if (tree == NULL) { // the tree is empty
        return newNode; // this is now the root node
    } else {
        return avlInsertRecursive(tree, newNode);
    }
}

//---------------------------------------------------------------------
AvlTreeNode * avlDelete(AvlTreeNode * tree, ElementType key) {
    return tree;
}

//---------------------------------------------------------------------
AvlTreeNode * avlFind  (AvlTreeNode * tree, ElementType key) {
    if(tree == NULL){
        return NULL;
    }
    else if(tree->data == key){
        return tree;
    }
    else if(tree->data>key){
        return avlFind(tree->leftTree, key);
    }
    else{
        return avlFind(tree->rightTree, key);
    }
}

//---------------------------------------------------------------------
void avlPrint(AvlTreeNode * tree) {
    printf("avlTree values in-order:\n");
    avlPrintRecursive(tree);
    printf("\n");
}

void avlPrintRecursive(AvlTreeNode * tree) {
    if (tree != NULL) {
        avlPrintRecursive(tree->leftTree);
        if (tree->leftTree == NULL) {
            printf("LEFT: NULL       ");
        } else {
            AvlTreeNode * left = tree->leftTree;
            printf("LEFT: %2d (%2d,%2d) ", left->data, left->height, left->heightDifference);
        }
        printf("Node: %2d (%2d,%2d) ", tree->data, tree->height, tree->heightDifference);
        if (tree->rightTree == NULL) {
            printf("RIGHT: NULL\n");
        } else {
            AvlTreeNode * right = tree->rightTree;
            printf("RIGHT: %2d (%2d,%2d)\n", right->data, right->height, right->heightDifference);
        }
        avlPrintRecursive(tree->rightTree);
    }
}

//---------------------------------------------------------------------
int avlVerifyHeightBalanced(AvlTreeNode * tree) {
    if (tree == NULL) {
        return 0;
    } else {
        int leftHeight = avlVerifyHeightBalanced(tree->leftTree);
        int rightHeight = avlVerifyHeightBalanced(tree->rightTree);

        if (leftHeight > rightHeight) {
            tree->height = leftHeight + 1;
        } else {
            tree->height = rightHeight + 1;
        }
        int difference = abs(leftHeight - rightHeight);
        if (difference > 1) {
            printf("Tree is not balanced. For node %d, height difference = %d\n", tree->data, difference);
        }
        return tree->height;
    }
}

//---------------------------------------------------------------------
int avlTreeHeight(AvlTreeNode * tree) {
    int height = 0;
    if (tree == NULL) {
        return 0;
    } else {
        int leftHeight = avlTreeHeight(tree->leftTree);
        int rightHeight = avlTreeHeight(tree->rightTree);
        if (leftHeight > rightHeight) {
            height = leftHeight + 1;
        } else {
            height = rightHeight + 1;
        }
        tree->height = height;
        tree->heightDifference = abs(leftHeight - rightHeight);
        return height;
    }
}

//---------------------------------------------------------------------
AvlTreeNode * avlInsertRecursive(AvlTreeNode * tree, AvlTreeNode * newNode ) {

//    if(tree == NULL){
//        tree = newNode;
//        return tree;
//    }
//    else{
//        if(tree->data < newNode->data){
//            tree->rightTree = avlInsertRecursive(tree, newNode);
//        }
//        else if(tree->data > newNode->data){
//            tree->leftTree = newNode;
//        }
//    }

    AvlTreeNode * newRoot;

    if (newNode->data < tree->data){
        if(tree->leftTree == NULL){
            tree->leftTree = newNode;
        }
        else{
            tree->leftTree = avlInsertRecursive(tree->leftTree, newNode);
        }
    }
    else{
        if(tree->rightTree == NULL){
            tree->rightTree = newNode;
        }
        else{
            tree->rightTree = avlInsertRecursive(tree->rightTree, newNode);
        }
    }

    return tree;
}

//---------------------------------------------------------------------
int getTreeHeightDifference(AvlTreeNode * tree) {
    if (tree == NULL) {
        return -1;  // An invalid difference
    } else {
        return tree->heightDifference;
    }
}

//---------------------------------------------------------------------
void leftRotate(AvlTreeNode * a, AvlTreeNode * b, AvlTreeNode * c) {

    AvlTreeNode * bLeft = b->leftTree;
    b->leftTree = a;
    a->rightTree = bLeft;
}

//---------------------------------------------------------------------
void rightRotate(AvlTreeNode * a, AvlTreeNode * b, AvlTreeNode * c) {
    AvlTreeNode * bRight = b->rightTree;
    b->rightTree = c;
    c->leftTree = bRight;
}

//---------------------------------------------------------------------
AvlTreeNode * avlRebalance(AvlTreeNode * tree) {
    // This function is only called if tree->heightDifference == 2
    // Execute the correct rotation out of 4 cases possible.

    return NULL;  // the new root of the sub-tree
}

//---------------------------------------------------------------------
int getTreeHeight(AvlTreeNode * tree) {
    if (tree == NULL) {
        return 0;
    } else {
        return tree->height;
    }
}

//---------------------------------------------------------------------
void avlUpdateHeight(AvlTreeNode * tree) {
    int height;
    int leftHeight = getTreeHeight(tree->leftTree);
    int rightHeight = getTreeHeight(tree->rightTree);
    if (leftHeight > rightHeight) {
        height = leftHeight + 1;
    } else {
        height = rightHeight + 1;
    }
    tree->height = height;
    tree->heightDifference = abs(leftHeight - rightHeight);
}

//---------------------------------------------------------------------
// Define a queue to print the tree one level at a time.
#define MAX_NODES  128
AvlTreeNode * queue[MAX_NODES];
int           queueFirst;
int           queueLast;
// Assuming a maximum tree height of 5, here is the spacing for each line.
int initialSpace[6] = {60, 28, 12, 4, 0, 0};
int innerSpace[6] = {0, 56, 24, 8, 0, 0};

void avlPrintRecursive2(AvlTreeNode * tree) {
    int level = 0;
    int nodesPerLevel = 1;
    int nodesThisLevel = 0;
    int lineDivisor = 2;
    int numSpaces = 80 / lineDivisor;
    int nodesPrintedThisLevel = 0;
    queueFirst = 0;
    queueLast = 1;
    queue[queueFirst] = tree;
    int divisor = 2;
    while (queueFirst < queueLast && queueFirst < MAX_NODES) {
        // Get the node on the queue, print it
        AvlTreeNode * node = queue[queueFirst++];

        // print the spaces between nodes
        if (nodesThisLevel == 0) {
            numSpaces = initialSpace[level];
        } else {
            numSpaces = innerSpace[level];
        }
        for (int s=0; s<numSpaces; s++) {
            printf(" ");
        }
        // print the node
        if (node != NULL) {
            nodesPrintedThisLevel++;
            printf("%2d (%1d,%1d)", node->data, node->height, getTreeHeightDifference(node));
            // Put all this node's children on the stack. Put NULL for missing children.
            queue[queueLast++] = node->leftTree;
            queue[queueLast++] = node->rightTree;
        } else {
            printf("--------");
            // Put two place holders on the stack for these missing children.
            queue[queueLast++] = NULL;
            queue[queueLast++] = NULL;
        }

        nodesThisLevel++;
        if (nodesThisLevel >= nodesPerLevel) {
            level++;
            printf("\n");
            if (nodesPrintedThisLevel == 0 || level > 5) break;
            nodesPerLevel *= 2;
            nodesThisLevel = 0;
            divisor += 1;
            numSpaces = (80 - (8 * nodesPerLevel)) / divisor ;
            nodesPrintedThisLevel = 0;
        }
    }
}

//int getTreeHeight(AvlTreeNode * tree) {
//    if (tree == NULL) {
//        return 0;
//    } else {
//        return tree->height;
//    }
//}
//
////---------------------------------------------------------------------
//void avlUpdateHeight(AvlTreeNode * tree) {
//    int leftHeight = getTreeHeight(tree->leftTree);
//    int rightHeight = getTreeHeight(tree->rightTree);
//    if (leftHeight > rightHeight) {
//        tree->height = leftHeight + 1;
//    } else {
//        tree->height = rightHeight + 1;
//    }
//}