//
// Created by Wayne.Brown on 3/6/2017.
//

#include <stdio.h>
#include <stdlib.h>
#include "AVlTree.h"

int main() {
    AvlTreeNode *tree;

    AvlTreeNode * newNode = (AvlTreeNode *) malloc(sizeof(AvlTreeNode));

    tree = avlCreateTree();
    for (int j=0; j<100; j += 10) {
        printf("Inserting %d\n", j);
        tree=avlInsert(tree, j);
        avlPrintRecursive2(tree);
        avlVerifyHeightBalanced(tree);
    }
}
