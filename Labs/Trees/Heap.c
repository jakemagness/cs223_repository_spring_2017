/** heapSort.c
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Section:
 * Project: Lab 30
 * Purpose: Implement a heapSort
 * ===========================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Heap.h"

#define MAX_BYTES_PER_ELEMENT 1000

static void convertToMaxHeap(Heap * heap);
static void heapSort(int[], int);
static void shiftDown(Heap * heap, int last, int root);

Heap * maxHeapCreate(void * array, int arrayCapacity,
                     int numberElements, int bytesPerElement,
                     greaterThanFunction func) {
    Heap * heap;

    heap = (Heap *) malloc(sizeof(Heap));

    // The heap points to the application's array
    heap->array = array;
    heap->compare = func;
    heap->arrayCapacity = arrayCapacity;
    heap->numberElements = numberElements;
    heap->bytesPerElement = bytesPerElement;
    heap->array = array;

    if (bytesPerElement > MAX_BYTES_PER_ELEMENT) {
        printf("Error in maxHeapCreate, the swap buffer is too small\n");
        exit(1);
    }

    convertToMaxHeap(heap);

    return heap;
}

void   maxHeapDelete(Heap *heap) {
    free(heap);
}

void * maxHeapRemoveMaximumElement(Heap *heap) {
    unsigned char temp[heap->bytesPerElement];
    // swap the root value with the last value
    void * root = heap->array + heap->bytesPerElement; // [1]
    void * last = heap->array + heap->numberElements * heap->bytesPerElement;

    memcpy(&temp, last, heap->bytesPerElement);
    memcpy(last,  root, heap->bytesPerElement);
    memcpy(root, &temp, heap->bytesPerElement);

    // Make the heap size one less
    heap->numberElements--;

    // Shift the root value to its proper place in the array
    shiftDown(heap, heap->numberElements, 1);

    // return the address of the last value
    return last;
}

void   maxHeapInsertNewElement(Heap *heap, void *element) {

}

/** -------------------------------------------------------------------
 * Convert an array of values to a max-heap
 * @param heap - the heap to convert
 */
void convertToMaxHeap(Heap * heap) {

    int n = heap->numberElements;

    // Note that the leaf nodes don't need to be shifted down,
    // so we start at the parent of the last node.
    for (int root = n / 2; root >= 1; root--) {
        shiftDown(heap, n, root);
    }
}

/** -------------------------------------------------------------------
 * Given a array that is a heap, except the root node does not
 * have the correct heap relationship to its children, shift
 * the value in the root node down until the heap is valid.
 * @param array - the array that holds the max-heap
 * @param last - the index of the last value in the max-heap;
 *               the heap is in indexes [1] to [last].
 * @param root - the index of the node that needs updating
 */
static void shiftDown(Heap * heap, int last, int root ) {
    unsigned char key[MAX_BYTES_PER_ELEMENT];
    void * rootAddress = heap->array + root * heap->bytesPerElement;

    // Copy the root value into the "key"
    memcpy(&key, rootAddress, heap->bytesPerElement);

    int child = 2 * root;
    void * childAddress = heap->array + child * heap->bytesPerElement;

    while (child <= last) { // While there is at least one child.

        if (child < last) { // There is a right child.
            // Find the bigger child.
            if (heap->compare(childAddress + heap->bytesPerElement, childAddress)) {
                child++;
                childAddress += heap->bytesPerElement;
            }
        }

        // 'child' holds the index of the bigger child node
        if (heap->compare(&key, childAddress)) {
            // The root node's value is bigger than both its children
            break;
        }

        // The key is smaller; promote num[child] to it's parent
        memcpy(rootAddress, childAddress, heap->bytesPerElement);

        // Move down the tree
        root = child;
        child = 2 * root;

        rootAddress  = heap->array + root * heap->bytesPerElement;
        childAddress = heap->array + child * heap->bytesPerElement;
    }
    memcpy(rootAddress, &key, heap->bytesPerElement);
} // end siftDown

