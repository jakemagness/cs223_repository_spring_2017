//
// Created by C18Jake.Magness on 1/12/2017.
//
#include <stdio.h>
#include <string.h>

char *my_strcpy(char *destination, char *source)
{
    int i = 0;
    char end = source[i];
    while(end != '\0')
    {
        destination[i] = source[i];
        i++;
        end = source[i];
    }
    destination[i] = '\0';
    return destination;
}
char *my_strcpy2(char *destination, char*source)
{
    while ((*destination++ = *source++) != '\0');

    return destination;
}
int main()
{
    char a[] = "Jake";
    char b[20];

    my_strcpy(b, a);

    printf("%s\n", b);

    return 0;
}