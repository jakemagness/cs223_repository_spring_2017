//
// Created by C18Jake.Magness on 3/21/2017.
//

#include <stdio.h>

#define K 10

typedef struct treeNode{
    int     data;
    struct treeNode * leftChild;
    struct treeNode * rightChild;
} TreeNode;

typedef struct treeNode2{
    int     data;
    struct treeNode2 * child[K];
} TreeNode2;


int treeHeight(TreeNode * tree){
    if(tree == NULL){
        return 0;
    }
    else{
        int leftHeight = treeHeight(tree->leftChild);
        int rightHeight = treeHeight(tree->rightChild);
        if(leftHeight>rightHeight){
            return leftHeight + 1;
        }
        else{
            return rightHeight + 1;
        }
    }
}

int treeHeight2(TreeNode2 * tree){
    if(tree == NULL){
        return 0;
    }
    else{
        int maxHeight = 0;
        for(int j = 0; j<K; j++){
            int subtreeHeight = treeHeight(tree->child[j]);
            if(subtreeHeight>maxHeight){
                maxHeight = subtreeHeight;
            }
        }
        return maxHeight + 1;
    }
}

int main() {

    return 0;
}