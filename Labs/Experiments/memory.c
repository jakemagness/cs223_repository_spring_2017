//
// Created by Wayne.Brown on 2/14/2017.
//

#include <stdlib.h>
#include <stdio.h>

int main() {
    int    count;
    void * address;
    void * address2;
    void * previousAddress;
    long   difference;
    int    size = 8;

    count = 0;
    address = (void *) 1;
    while (count < 5 && address != NULL) {
        address = malloc(size);
        address2 = malloc(size*2000);
        printf("%6d   address = %lu\n", count, (unsigned long) address);
        printf("%6d   address2 = %lu\n", count, (unsigned long) address2);

        realloc(address, size+5);
        realloc(address2, size*20000);
        printf("%6d   address = %lu\n", count, (unsigned long) address);
        printf("%6d   address2 = %lu\n", count, (unsigned long) address2);

        count++;

//        address = malloc(size);
//        printf("%6d   address = %lu\n", count, (unsigned long) address);
//        count++;
//
//        if (previousAddress != NULL){
//            difference = (long) address - (long) previousAddress;
//            printf("     %ld", difference);
//        }
//        printf("\n");
//        previousAddress = address;
    }
}