//
// Created by C18Jake.Magness on 1/10/2017.
//
#include <stdio.h>
#include <string.h>

int main() {
    char first[] = "Fred";
    char second[] = "Smith";
    char full_name[30] = "";
    char *a;

    strcat(full_name, first);
    strcat(full_name, " ");
    strcat(full_name, second);
    strcat(full_name, second);

    a = first;
    printf("a is %p\n", a);

    printf("%d\n", strcmp(first, second));

    printf("%s", first);
}
