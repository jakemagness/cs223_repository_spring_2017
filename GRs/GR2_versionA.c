/** GR2_versionA.c
 * ===========================================================
 * Name: Jake Magness             , Spring 2017
 * Section: M3-4
 * Project: GR 2
 * Purpose: Assessment on trees and hash tables
 * ===========================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Function definitions
void memory();
void bits(int gamma);
char * toBitStr(size_t const size, void const * const ptr);

//---------------------------------------------------------------------
// Main function, start of execution
int main() {
//    memory();
    bits(11);
}

//---------------------------------------------------------------------
// Problem 11:                                        ________ / 15 pts
void memory() {
    struct beta {
        char    c1;
        char    c2;
        int     i1;
        int     i2;
        double  d;
    };

    struct beta b;

    printf("Memory location of b: %lu\n", (unsigned long) &b);
    printf("Memory location of char c1: %lu\n", (unsigned long) &(b.c1));
    printf("Memory location of char c2: %lu\n", (unsigned long) &(b.c2));
    printf("Memory location of int i1: %lu\n", (unsigned long) &(b.i1));
    printf("Memory location of int i2: %lu\n", (unsigned long) &(b.i2));
    printf("Memory location of double d: %lu\n", (unsigned long) &(b.d));
}

//---------------------------------------------------------------------
// Problem 12:                                        ________ / 10 pts
void bits(int gamma) {
    //shift bits of gamma to the left 3 bits
    int shift = gamma<<3;
    printf("Gamma shifted left by three bits is: %s\n",toBitStr(sizeof(int), &shift));

    //Clearing the lowest Bit.
    int whichBit = 0;
    char * gammaString = toBitStr(sizeof(int), &gamma);
    int gammaClear = gamma & ~(1 << whichBit);
    printf("Cleared lowest bit: %s\n", toBitStr(sizeof(int), &gammaClear));

    //Complimenting the value.
    for (int j =0; j<strlen(gammaString); j++){
        if(gammaString[j] == '0'){
            gammaString[j] = '1';
        }
        else{
            gammaString[j] = '0';
        }
    }
    printf("Complimented String: %s\n", gammaString);
    gammaString[31] = '1';
    //Know this probably wouldn't work if we had a carry but I am running out of time to figure out how to convert
    // and reconvert.
    printf("Adding 1 to the value %s\n", gammaString);

    //Remove all high order bits
    //Gets 8.
    int power1 = 8;
    char lowest3[8];
    char * gammaString2 = toBitStr(sizeof(int), &gamma);
    for(int i = 0; i < power1; i++){
        lowest3[power1-i-1] = gammaString2[31-i];
    }
    printf("lowest3: %s\n", lowest3);



    //Raising a value to a power
    int power = 3;
    int gammaRaise = 2;
    //Shifting the bits to the left multiples the value by a power of 2.
    gammaRaise = gammaRaise<<gamma-1;
    printf("GammaRaise: %i\n", gammaRaise);
}


//---------------------------------------------------------------------
// Convert an ordinal value into a string of 0's and 1's.
// This assumes little endian byte ordering.
char * toBitStr(size_t const size, void const * const ptr)
{
    unsigned char *bytes = (unsigned char*) ptr;
    int bit;
    char * string = malloc(sizeof(char) * (size * 8 + 1));
    int k;

    k=0;
    for (int i = (int)size-1; i >= 0; i--) { // for each byte
        for (int j=7; j >= 0; j--) { // for each bit in this byte
            bit = (bytes[i] >> j) & 1;
            string[k++] = (char) ((bit == 1) ? '1' : '0');
        }
    }
    string[k] = '\0'; // null terminate the string

    return string;
}

int hashString(int hashTableArraySize, char * str) {
    int len = (int) strlen(str);
    char * p   = str;
    int    x   = *p << 7;
    while (*p != '\0') {
        x = 1000003 * x ^ *p;
        p++;
    }
    x = x ^ len;
    return x & (hashTableArraySize-1);
}