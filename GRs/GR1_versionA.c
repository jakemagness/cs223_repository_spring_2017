/** GR1_versionA.c
 * ===========================================================
 * Name: Jake Magness, Spring 2017
 * Section: M3
 * Project: GR 1
 * Purpose: Assessment on lists
 * ===========================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Exam{
    int var1;
    float var2;
    char var3[20];
}Exam;

// Function definitions
Exam * create();
int * makeArray(int numberElements);
void selectionSort(int *array, int arraySize);
void arrayListSelectionSort(int *array, int arraySize);
int findSmallest(int *array, int firstIndex, int lastIndex);

//---------------------------------------------------------------------
// Main function, start of execution
int main() {

// Tests that I used for number 1 and 2
    Exam *exam1 = create();

    printf("%p\n", exam1);
    printf("%s\n", exam1->var3);
    int * test = makeArray(10);

    for(int i = 0; i < 10; i++){
        printf("%i", test[i]);
    }

// Tests I used to determine if my algorithm was correct for the selectionSort().  My find smallest function does find
// the smallest value and the smallest values are moved to the front of the list.

    int * anArray = (int *) malloc(sizeof(int)*10);

    for(int i = 0; i < 10; i++){
        int number = (int) ((float) rand() / (float) RAND_MAX * 10);
        anArray[i] = number;
    }
    for(int j = 0; j < 10; j++){
        printf("%i", anArray[j]);
    }
    printf("\n");
    selectionSort(anArray, 10);
    for(int n = 0; n < 10; n++){
        printf("%i", anArray[n]);
    }

    //Find smallest tests.  It does print out the correct number.
//    printf("%i", anArray[findSmallest(anArray, 0, 10)]);
}

//---------------------------------------------------------------------
// Problem 15:                                        ________ / 14 pts
Exam * create() {
    Exam *problem1;

    problem1 = (Exam *) malloc(sizeof(Exam));

    problem1->var1 = 0;
    problem1->var2 = 0.0;
    strcat(problem1->var3, "Sam");

    return problem1;
}

//---------------------------------------------------------------------
// Problem 16:                                        ________ / 20 pts
int * makeArray(int numberElements) {
    //Allocating a block of array type the size of numberElements
    int * new_array = (int *) malloc(sizeof(int)*numberElements);
    int flag = 0;

    for(int i = 0; i < numberElements; i++){

// Utilizing the modulus operator was giving new_array[9] the wrong output.. changed to flag system to get proper output.
//        if(new_array[i]% 2 == 0){
//            new_array[i] = 0;
//        }
//        else{
//            new_array[i] = -1;
//        }
        if(flag == 0){
            new_array[i] = 0;
            flag = 1;
        }
        else{
            new_array[i] = -1;
            flag = 0;
        }
    }
    return new_array;
}

//---------------------------------------------------------------------
// Problem 17:                                        ________ / 20 pts

int findBiggest(int *array, int firstIndex, int lastIndex) {
    int biggestIndex = firstIndex;
    for (int j=firstIndex+1; j<=lastIndex; j++) {
        if (array[j] > array[biggestIndex]) {
            biggestIndex = j;
        }
    }
    return biggestIndex;
}

int findSmallest(int *array, int firstIndex, int lastIndex) {
    int smallestIndex = firstIndex;
    for (int j=firstIndex+1; j<=lastIndex; j++) {
        if (array[j] < array[smallestIndex]) {
            smallestIndex = j;
        }
    }
    return smallestIndex;
}

void swap(int *array, int j, int k) {
    int temp = array[j];
    array[j] = array[k];
    array[k] = temp;
}

void arrayListSelectionSort(int *array, int arraySize) {
    int indexOfBiggestValue;
    for (int lastIndex = arraySize-1; lastIndex >= 1; lastIndex--) {
        indexOfBiggestValue = findBiggest(array, 0, lastIndex);
        swap(array, indexOfBiggestValue, lastIndex);
    }
}

void selectionSort(int *array, int arraySize) {
    int indexOfSmallestValue;
    for (int firstIndex = 0; firstIndex < arraySize; firstIndex++) {
        indexOfSmallestValue = findSmallest(array, firstIndex, arraySize);
        swap(array, indexOfSmallestValue, firstIndex);
    }
}
