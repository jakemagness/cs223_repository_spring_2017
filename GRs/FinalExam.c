/** FinalExam.c
 * ===========================================================
 * Name: Jake Magness                , Spring 2017
 * Section: M3
 * Project: Final Exam
 * Purpose: Course assessment.
 * ===========================================================
 */

//---------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int   data;
    struct node * next;
    struct node * previous;
} Node;

typedef struct linkedList {
    Node * first;
    Node * last;
    int    numberElements;
} DL;

DL * dlCreate() {
    DL * my_list = NULL;

    my_list = (DL *) malloc(sizeof(DL));
    my_list->first = NULL;
    my_list->last = NULL;
    my_list->numberElements = 0;

    return my_list;
}

void dlDelete(DL * list){

    Node * next = NULL;

    while(list->first != NULL){
        next = list->first->next;
        free(list->first);
        list->first = next;
    }

    list->first = NULL;
    list->last = NULL;
    list->numberElements = 0;

    free(list);
}

void dlInsert(DL * list, int element, int position){

    Node * new = (Node * ) malloc(sizeof(Node));
    Node * node = (Node *) malloc(sizeof(Node));

    if(position == 0){
        new->next = list->first;
        list->first = new;
        new->data = element;
    }
    else{
        new = list->first;
        Node * temp = new->next;

        for (int i = 0; i < position; i++){
            temp = new;
            new = new->next;
        }

        node->data = element;
        temp->next = node;
        node->next = new;
        if (new != NULL){
            node->previous = temp;
        }

        list->last = new;
    }
    list->numberElements += 1;
}

void dlPrint(DL * list) {
    Node * address;
    address = list->first;

    for (int i = 0; i < list->numberElements; i++) {
        printf("%d ", address->data);
        address = address->next;
    }
}
void dlRemove(DL * list, int element){
    Node * target = list->first;
    Node * prev = list->first;
    Node * next = list->first->next;

    for(int i = 0; i < list->numberElements; i++){

        if(target->data == element && i == 0){
            next = list->first->next;
            free(list->first);
            list->first = next;
            list->numberElements -= 1;
        }
        else if(target->data == element){
            prev->next = next;
            next->previous = prev;
            free(target);
            list->numberElements -= 1;
        }
        else{
            prev = target;
            target = target->next;
            if(next == NULL){
                next = NULL;
            }
            else{
                next = next->next;
            }
        }
    }
}

int main() {
    DL * list = dlCreate();

    dlInsert(list, 10, 0);
    dlPrint(list);            // result {10}
    printf("\n");
    dlInsert(list, 20, 1);
    dlPrint(list);              // result {10, 20}
    printf("\n");
    dlInsert(list, 30, 1);
    dlPrint(list);            // result {10, 30, 20}
    printf("\n");
    dlInsert(list, 40, 0);
    dlPrint(list);            // result {40, 10, 30, 20}
    printf("\n");

    dlRemove(list, 10);
    dlPrint(list);            // result {40, 30, 20}
    printf("\n");
    dlRemove(list, 70);
    dlPrint(list);            // result {40, 30, 20}
    printf("\n");
    dlRemove(list, 30);
    dlPrint(list);            // result {40, 20}
    printf("\n");
    dlRemove(list, 40);
    dlPrint(list);            // result {20}
    printf("\n");
    dlRemove(list, 20);
    dlPrint(list);            // result {}
    printf("\n");
    dlDelete(list);
}